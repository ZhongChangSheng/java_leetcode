package cn.leet_code;

/**
 * 给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。
 *
 * 如果反转后整数超过 32 位的有符号整数的范围 [−231,  231 − 1] ，就返回 0。
 *
 * 假设环境不允许存储 64 位整数（有符号或无符号）。

 */
public class fz_int {
    public int reverse(int x) {
       int res=0;
       while(x!=0){
           int t=x%10;
          int newRes=res*10+t;
          if((newRes-t)/10!=res)
              return 0;
          res=newRes;
          x=x/10;
       }
       return res;
    }

    public static void main(String[] args) {
        fz_int f=new fz_int();
        System.out.println(f.reverse(-121));
    }
}
/**
 // 先取符号
 public class fz_int {
             public int reverse(int x) {
                 int op = x>0?1:-1;
                 x = Math.abs(x);
                 String s = new StringBuffer(String.valueOf(x)).reverse().toString();
                 String sMax = String.valueOf(Integer.MAX_VALUE);
                int length = sMax.length();
                if(s.length()>=length && s.compareTo(sMax)>0){
                return 0;
 }
 return Integer.valueOf(s) *op;
 }
 */