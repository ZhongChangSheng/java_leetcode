package cn.leet_code;

/**
 * 编写一个函数来查找字符串数组中的最长公共前缀。
 *
 * 如果不存在公共前缀，返回空字符串 ""。
 */
public class MaxLength {
    public String longestCommonPrefix(String[] strs) {
           if(strs==null||strs.length==0)
               return "";
           String pre=strs[0];
           int i=1;
           while (i<strs.length){
               while(strs[i].indexOf(pre)!=0){
                   pre=pre.substring(0,pre.length()-1); }
                   i++;

           }
           return pre;
    }

    public static void main(String[] args) {
        MaxLength m=new MaxLength();
        String[] strs={"flower","flow","flight"};
        System.out.println(m.longestCommonPrefix(strs));
    }
}
