package cn.leet_code;

/**
 * 给定一个数组 prices ，它的第 i 个元素 prices[i] 表示一支给定股票第 i 天的价格。
 *
 * 你只能选择 某一天 买入这只股票，并选择在 未来的某一个不同的日子 卖出该股票。设计一个算法来计算你所能获取的最大利润。
 * 返回你可以从这笔交易中获取的最大利润。如果你不能获取任何利润，返回 0 。
 * dp[i][0]:当天未持有股票  =max(dp[i-1][0],dp[i-1][1]+prices[i])
 * dp[i][1]:当天持有股票    =max(dp[i-1][1],-prices[i])
 */
public class MaxProfit {
    public int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0)
            return 0;
//        int length=prices.length;
//        int dp[][]=new int[length][2];
//        dp[0][0]=0;
//        dp[0][1]=-prices[0];
//        for(int i=1;i<length;i++){
//            dp[i][0]=Math.max(dp[i-1][0],dp[i-1][1]+prices[i]);
//            dp[i][1]=Math.max(dp[i-1][1],-prices[i]);
//        }
//        return dp[length-1][0];
        //代码优化
        int length=prices.length;
        int noHold=0;
        int hold=-prices[0];
        for(int i=1;i<length;i++){
            noHold=Math.max(noHold,hold+prices[i]);
            hold=Math.max(hold,-prices[i]);
        }
        return noHold;
    }
}
