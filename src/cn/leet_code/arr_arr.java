package cn.leet_code;

import java.util.Arrays;

/**
 * 给定两个数组，编写一个函数来计算它们的交集。
 */
public class arr_arr {
    public int[] intersect(int nums1[],int nums2[]){
        //1.先进行排序
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        int len=nums1.length<nums2.length?nums1.length:nums2.length;
        int[] arr=new int[len];
        int i=0,j=0,k=0;
        if(nums1.length==0||nums2.length==0)
            return arr;
        while (i<nums1.length&&j<nums2.length)
        {
            if(nums1[i]==nums2[i])
            {
                arr[k++]=nums1[i];
                i++;
                j++;
            }
            else if(nums1[i]<nums2[i])
            {
                i++;
            }
            else
                j++;
        }
           return Arrays.copyOfRange(arr, 0, k);
    }
}
