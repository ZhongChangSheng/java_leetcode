package cn.leet_code;

/**
 * 给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。
 * a^a=0；自己和自己异或等于0
 a^0=a；任何数字和0异或还等于他自己
 a^b^c=a^c^b；异或运算具有交换律
 */
public class only_once {
    public int singleNumber(int[] nums) {
        int result=0;
        for(int i=0;i<nums.length;i++){
            result=result^nums[i];
        }
        return result;
    }
}
