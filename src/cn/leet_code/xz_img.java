package cn.leet_code;

/**
 * 给定一个 n × n 的二维矩阵 matrix 表示一个图像。请你将图像顺时针旋转 90 度。
 *
 * 你必须在 原地 旋转图像，这意味着你需要直接修改输入的二维矩阵。请不要 使用另一个矩阵来旋转图像
 *
 */
public class xz_img {
    public void rotate(int[][] matrix) {
        //1.先上下交换
        for(int i=0;i<matrix.length/2;i++)
        {
            int tem[]=matrix[i];
            matrix[i]=matrix[matrix.length-i-1];
            matrix[matrix.length-i-1]=tem;
        }
        //2.对角线交换
        for(int i=0;i<matrix.length-1;++i)
            for(int j=i+1;j<matrix.length;j++)
            {
                int tem=matrix[i][j];
                matrix[i][j]=matrix[j][i];
                matrix[j][i]=tem;
            }
    }
}
