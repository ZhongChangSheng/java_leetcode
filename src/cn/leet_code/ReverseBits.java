package cn.leet_code;

/**
 * 颠倒给定的 32 位无符号整数的二进制位。
 */
public class ReverseBits {
    public int reverseBits(int n) {
        int res=0;
        for(int i=0;i<32;i++){
            res<<=1;//先左移，最后一个位置存储最后一位
            res+=n&1;
            n>>=1;//n往右移，去掉最后一位
        }
        return res;
    }
}
