package cn.leet_code;

/**
 * 假设你有 n 个版本 [1, 2, ..., n]，你想找出导致之后所有版本出错的第一个错误的版本。
 *
 * 你可以通过调用 bool isBadVersion(version) 接口来判断版本号 version 是否在单元测试中出错。实现一个函数来查找第一个错误的版本。你应该尽量减少对调用 API 的次数。

 */
public class HalfFind {
    public int firstBadVersion(int n) {
           int start=1;
           int end=n;
           //int mid=(start+end)/2;//二分查找不要用，会溢出，两数相加可能超出范围
        //int mid1=start+(end-start)/2;
        while(start<end){
            int mid=start+(end-start)/2;

            if(!isBadVersion(mid)){
                start=mid+1;
            }
            else{
                end=mid;

            }
        }
        return start;

    }

    private boolean isBadVersion(int n) {
        int bad=4;
        if(n==bad)
            return true;
        else
            return false;

    }
}
