package cn.leet_code;

public class KMP {
    public int strStr(String haystack, String needle) {
        if(needle.length()==0)
            return 0;
        int i=0,j=0;
        int[] next=new int[needle.length()];
        getNext(needle,next);
        while (i<haystack.length()&&j<needle.length())
        {
            if(j==-1||haystack.charAt(i)==needle.charAt(j)){
                i++;
                j++;

            }
            else {
                j=next[j];
            }
            if(j==needle.length())
                return i-j;

        }
        return -1;
    }
    private void getNext(String s,int next[]){
        char chars[]=s.toCharArray();
        int len=chars.length;
        int i=0,j=-1;
        next[0]=-1;
        while (i<len-1){
            if(j==-1||chars[i]==chars[j]){
                i++;
                j++;
                next[i]=j;
            }else {
                j=next[i];
            }
        }

    }
}
