package cn.leet_code;
/**
 * 给定一个正整数 n ，输出外观数列的第 n 项。
 *
 * 「外观数列」是一个整数序列，从数字 1 开始，序列中的每一项都是对前一项的描述。

 */
public class AndSay{
    public String countAndSay(int n){
        if(n==1)
            return "1";
        String s1 = countAndSay(n - 1);
        StringBuilder s=new StringBuilder();
        char c=s1.charAt(0);
        int count=0;//记录数字出现的次数
        for (int i=0;i<s1.length();i++){
            if(s1.charAt(i)==c){
                count++;
            }else{
                s.append(count);
                s.append(c);
                c=s1.charAt(i);
                count=1;
            }
        }
        s.append(count);
        s.append(c);
        return s.toString();
    }

    public static void main(String[] args) {
        int n=4;
        AndSay a=new AndSay();


        System.out.println(a.countAndSay(n));
    }
}
