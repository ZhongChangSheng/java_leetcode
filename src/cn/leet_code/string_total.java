package cn.leet_code;

/**
 * 给定一个字符串，找到它的第一个不重复的字符，并返回它的索引。如果不存在，则返回 -1。
 */
public class string_total {
    public int firstUniqChar(String s) {
        //统计字母出现次数
        int count[]=new int[26];
         char chars[]=s.toCharArray();
         for(int i=0;i<chars.length;i++)
         {
             count[chars[i]-'a']++;
         }
         for(int i=0;i<chars.length;i++)

             if(count[chars[i]-'a']==1)
                 return i;
             return -1;

    }

    public static void main(String[] args) {
        string_total a=new string_total();
        String s="abcdea";
        System.out.println(a.firstUniqChar(s));
    }


}
