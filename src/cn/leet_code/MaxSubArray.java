package cn.leet_code;

/**
 * 给定一个整数数组 nums ，找到一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。
 * dp[i]用来表示，终点在i的子序列的最佳子序和，这样dp[i]和dp[i-1]之间便有简单明了的关系。这时需要用到贪心法。
 * 此时，最后返回的值应该从dp数组中取最大值，即以i结尾的数组的最大子序和中找到最大的，作为整个数组的最佳子序和。
 * dp[i] = Math.max(dp[i - 1], 0) + num[i];

 */
public class MaxSubArray {
    public int maxSubArray(int[] nums) {
//        int length=nums.length;
//        int[] dp=new int[length];
//        dp[0]=nums[0];
//        int max=dp[0];
//        for(int i=1;i<length;i++){
//            dp[i]=Math.max(dp[i-1],0)+nums[i];
//            max=Math.max(dp[i],max);
//        }
//        return max;
        //代码优化
        int length=nums.length;
        int total=nums[0];
        int max=total;
        for(int i=1;i<length;i++){
            total=Math.max(total,0)+nums[i];
            max=Math.max(max,total);
        }
        return max;
    }
}
