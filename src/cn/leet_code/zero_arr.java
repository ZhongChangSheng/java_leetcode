package cn.leet_code;

/**
 * 给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
 */
public class zero_arr {
    public void moveZeroes(int[] nums) {
        int count=0;//记录0的个数
        int next=0;//定义next用来记录非零元素
        for(int i=0;i<nums.length;i++)
        {
            if(nums[i]==0)
                count++;
            else
                nums[next++]=nums[i];
        }
        for(int j=nums.length-1;j>=nums.length-count;j--)
        {
            nums[j]=0;
        }

    }
}
