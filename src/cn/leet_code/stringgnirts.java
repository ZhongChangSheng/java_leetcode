package cn.leet_code;

/**
 * 给定一个字符串，验证它是否是回文串，只考虑字母和数字字符，可以忽略字母的大小写。
 */
public class stringgnirts {
    public boolean isPalindrome(String s) {
       s = s.replaceAll("[^A-Za-z0-9]", "").toLowerCase();
        char a[]=s.toCharArray();
        for(int i=0;i<a.length/2;i++)
            if(a[i]!=a[a.length-i-1])
                return false;

        return true;
    }

    public static void main(String[] args) {
        stringgnirts st=new stringgnirts();
        String s="race a car";
        System.out.println(st.isPalindrome(s));
    }
}
