package cn.leet_code;

import java.util.Arrays;

/**
 * 给定一个整数数组，判断是否存在重复元素。
 *
 * 如果存在一值在数组中出现至少两次，函数返回 true 。如果数组中每个元素都不相同，则返回 false 。
 *
 *
 */
public class reset_arr {
    public boolean containsDuplicate(int[] nums){
        //1.先排序
        Arrays.sort(nums);
        for(int i=0;i<nums.length-1;i++){
            if(nums[i]==nums[i+1]){
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        reset_arr re=new reset_arr();
        int nums[]={1,2,4,3};
        System.out.println(re.containsDuplicate(nums));
    }
}
