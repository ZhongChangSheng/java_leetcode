package cn.leet_code;

/**
 *你是一个专业的小偷，计划偷窃沿街的房屋。每间房内都藏有一定的现金，
 * 影响你偷窃的唯一制约因素就是相邻的房屋装有相互连通的防盗系统，如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警。
 *
 * 给定一个代表每个房屋存放金额的非负整数数组，计算你 不触动警报装置的情况下 ，一夜之内能够偷窃到的最高金额
 dp[i][0]：代表第i+1天没有偷窃,那么第i天是否偷窃都可以 =max(dp[i-1][0],dp[i-1][1]);
 dp[i][1]:代表第i+1天进行了偷窃,那么第i天不能偷窃 =dp[i-1][0]+nums[i];

 */
public class Rob {
    public int rob(int[] nums) {
        int len=nums.length;
        int[][]dp=new int[len][2];
        dp[0][0]=0;
        dp[0][1]=nums[0];
        for(int i=1;i<len;i++){
            dp[i][0]=Math.max(dp[i-1][0],dp[i-1][1]);
            dp[i][1]=dp[i-1][0]+nums[i];
        }
        return Math.max(dp[len-1][0],dp[len-1][1]);
    }
}
