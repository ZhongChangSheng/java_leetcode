package cn.leet_code;


import java.util.ArrayList;
import java.util.List;

/**
 * 给定一个非负整数 numRows，生成「杨辉三角」的前 numRows 行。
 *
 * 在「杨辉三角」中，每个数是它左上方和右上方的数的和。
 */
public class yhsj {
    public void generate() {
        int numRows=5;
        List<List<Integer>> res=new ArrayList<>();
        List<Integer> rows=new ArrayList<>();
        for(int i=0;i<numRows;i++){
            rows.add(0,1);
            for(int j=1;j<rows.size()-1;j++)
                rows.set(j,rows.get(j)+rows.get(j+1));
            res.add(new ArrayList<>(rows));

        }
        System.out.println(res);

    }
}
