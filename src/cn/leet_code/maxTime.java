package cn.leet_code;
import java.util.*;
/**
 * 给定一个数组 prices ，其中 prices[i] 是一支给定股票第 i 天的价格。
 *
 * 设计一个算法来计算你所能获取的最大利润。你可以尽可能地完成更多的交易（多次买卖一支股票）。
 */
public class maxTime {
    public int maxProfit(int [] prices){
          if(prices==null||prices.length==0)
              return 0;
          int total=0;
          for(int i=0;i<prices.length-1;i++)
              total=total+Math.max(prices[i+1]-prices[i],0);
              return  total;

    }

}

