package cn.leet_code;

/**
 * 双指针（删除有序数组的重复数)
 * 使用两个指针，右指针始终往右移动，
 *
 * 如果右指针指向的值等于左指针指向的值，左指针不动。
 * 如果右指针指向的值不等于左指针指向的值，那么左指针往右移一步，然后再把右指针指向的值赋给左指针。
 */
public class re_arr2 {
    public  int  removeDuplicates(int [] A){
        //边界条件判断
        if(A==null||A.length==0)
            return 0;
        //初始化left
        int left=0;
        //如果左指针和右指针指向的值一样，说明有重复的，
        //            //这个时候，左指针不动，右指针继续往右移。如果他俩
        //            //指向的值不一样就把右指针指向的值往前挪
        for(int right=1;right<A.length;right++)
            if(A[left]!=A[right])
                    A[++left]=A[right];
        return ++left;
    }
}
