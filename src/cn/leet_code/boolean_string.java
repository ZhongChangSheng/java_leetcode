package cn.leet_code;

/**
 给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。

 注意：若 s 和 t 中每个字符出现的次数都相同，则称 s 和 t 互为字母异位词。

 */
public class boolean_string {
    public boolean isAnagram(String s, String t) {
        //1.两字符串转换为数组
        char chars[]= s.toCharArray();
         char chart[]=t.toCharArray();
         //2.记录每个字母出现的次数，s出现-t出现
         int count[]=new int[26];

         if(chars.length!=chart.length)
             return false;
         for(int i=0;i<chars.length;i++)
         {
             count[chars[i]-'a']++;
         }
        for(int i=0;i<chart.length;i++)
        {

            if(count[chart[i]-'a']==0)
                return false;
            count[chart[i]-'a']--;
        }
        return true;
        /*for(int j:count)
            if(j!=0)
                return false;
            return true;*/



    }
}
