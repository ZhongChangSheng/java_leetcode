package cn.leet_code;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class 杨辉三角 {
    public static List<Integer> getRow(int rowIndex) {
           Integer []dp=new Integer[rowIndex+1];
           Arrays.fill(dp,1);
           for(int i=2;i<=rowIndex;i++){
               for(int j=i-1;j>0;j--){
                   dp[j]=dp[j-1]+dp[j];
               }
           }
           List<Integer> res=Arrays.asList(dp);
             return res;
    }

    public static void main(String[] args) {
        System.out.println(getRow(0));
    }
}
