package cn.LeetCode.第二周.排序;

import java.util.Arrays;

public class 用最少数量的箭引爆气球452 {
    public static void main(String[] args) {
        //搞人心态这个样例,所以需要改进
        int[][] p={{-2147483646,-2147483645},{2147483646,2147483647}};
        //int[][]p={{1,2},{3,4}};
        System.out.println(findMinArrowShots(p));
    }
    public static int findMinArrowShots(int[][] points) {
        //Arrays.sort(points, (a, b) -> a[0] - b[0]);
        // 这个排不了，需要改进 int[][] p={{-2147483646,-2147483645},{2147483646,2147483647}};
        //改进后
        Arrays.sort(points, (a, b) -> a[0] < b[0] ? -1 : 1);
        int pre=0;
        int last=1;
        int shoot=1;
        while(pre<points.length-1&&last<points.length){
            if(points[pre][1]>=points[last][0]){
                last++;
            }
            else{
                shoot++;
                pre=last;
                last++;
            }
        }
        return shoot;
    }
}
