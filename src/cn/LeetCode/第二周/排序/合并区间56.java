package cn.LeetCode.第二周.排序;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class 合并区间56 {
    public int[][] merge(int[][] intervals) {
        List<int[]> res = new ArrayList<>();
        if (intervals.length == 0 || intervals == null){
            return res.toArray(new int[0][]);
        }
        // 对起点终点进行排序,使用lambda表达式，
        Arrays.sort(intervals, (a, b) -> a[0] - b[0]);
        int i = 0;
        while (i < intervals.length) {
            int left = intervals[i][0];
            int right = intervals[i][1];
            // 如果有重叠，循环判断哪个起点满足条件
            while (i < intervals.length - 1 && right>=intervals[i+1][0]) {
                i++;
                right = Math.max(right, intervals[i][1]);
            }
            // 将现在的区间放进res里面
            int []a=new int[2];
            a[0]=left;
            a[1]=right;
            res.add(a);
            // 接着判断下一个区间
            i++;
        }
        return res.toArray(new int[0][]);
//        List<int []>list=new ArrayList<>();
//        Arrays.sort(intervals, (a, b) -> a[0] - b[0]);
//          for(int i=0;i<intervals.length-1;){
//              int start=intervals[i][0];
//              int end=intervals[i][1];
//              if(end>=intervals[i+1][0]&&end<intervals[i+1][1]){
//                  intervals[i][0]=start;
//                  intervals[i][1]=intervals[i+1][1];
//                  int []a=new int[2];
//                  a[0]=intervals[i][0];
//                  a[1]=intervals[i][1];
//                  list.add(a);
//                  i=i+2;
//              }
//              else if(end>=intervals[i+1][0]&&end>=intervals[i+1][1]) {
//                  int[] a = new int[2];
//                  a[0] = intervals[i][0];
//                  a[1] = intervals[i][1];
//                  list.add(a);
//                  i++;
//              }
//              else if(end<intervals[i+1][0]){
//                  int []a=new int[2];
//                  a[0]=intervals[i][0];
//                  a[1]=intervals[i][1];
//                  list.add(a);
//                  i++;
//                  if(i==intervals.length-1){
//                      int []b=new int[2];
//                      b[0]=intervals[i][0];
//                      b[1]=intervals[i][1];
//                      list.add(b);
//                  }
//              }
//          }
//         return list.toArray(new int [0][]);
    }
}
