package cn.LeetCode.第二周.排序;

public class b选择排序 {
    public static void main(String[] args) {
        int nums[]={1,3,5,4,8};
        xzPX(nums);
    }
    public static void xzPX(int[] nums){
        int n=nums.length;
        int minIndex;//记录未排序区域的最小值下标
        for(int i=0;i<n-1;i++){
            minIndex=i;
            for(int j=i+1;j<n;j++){
                  if(nums[j]<nums[minIndex]){
                      minIndex=j;//更新下标
                  }
            }
            if(i!=minIndex){
                //表明需要交换
                int tem=nums[minIndex];
                nums[minIndex]=nums[i];
                nums[i]=tem;
            }

        }
        for (int num : nums) {
            System.out.print(num+" ");
        }
    }
}
