package cn.LeetCode.第二周.排序;

public class d快速排序 {
    public static void main(String[] args) {
        int[] nums={5,2,4,3,1,8,6};
        KSort(nums,0,nums.length-1);
        for (int num : nums) {
            System.out.print(num+" ");
        }
    }
   public static void KSort(int[]nums,int begin,int end){
       if(begin>end){
           return;
       }
       int tem=nums[begin];//记录基准值
       int i=begin;
       int j=end;
       while(i!=j){
           while (nums[j]>=tem&&j>i){
               j--;
           }
           while(nums[i]<=tem&&i<j){
               i++;
           }
           if(j>i){
               int t=nums[i];
               nums[i]=nums[j];
               nums[j]=t;
           }
       }
       nums[begin]=nums[i];
       nums[i]=tem;
       //一轮交换完后，左边的数都<基值<右边的数都
       KSort(nums,begin,i-1);
       KSort(nums,i+1,end);
   }
}
