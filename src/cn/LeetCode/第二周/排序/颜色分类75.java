package cn.LeetCode.第二周.排序;

public class 颜色分类75 {
    //单指针法，需要遍历两次
    //第一次将所有的0放到最前面
    //第二次将所有的1放到0的后面
    public void sortColors(int[] nums) {
        int ptr=0;
        for(int i=0;i<nums.length;i++){
            if(nums[i]==0){
                int temp=nums[i];
                nums[i]=nums[ptr];
                nums[ptr]=temp;
                ptr++;
            }
        }
        for(int i=ptr;i<nums.length;i++){
            if(nums[i]==1){
                int tem=nums[i];
                nums[i]=nums[ptr];
                nums[ptr]=tem;
                ptr++;
            }
        }

    }
    //双指针法
    public void sortColors2(int[] nums) {
        int prt=0,prt1=0;
        for(int i=0;i<nums.length;i++){
            if(nums[i]==0){
                int temp=nums[i];
                nums[i]=nums[prt];
                nums[prt]=temp;
                if(prt<prt1){
                    int tem=nums[i];
                    nums[i]=nums[prt1];
                    nums[prt1]=tem;
                }
                prt++;
                prt1++;
            }else if(nums[i]==1){
                int temp=nums[i];
                nums[i]=nums[prt1];
                nums[prt1]=temp;
                prt1++;
            }

        }
    }
}
