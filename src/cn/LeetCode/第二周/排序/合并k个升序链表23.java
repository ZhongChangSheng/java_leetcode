package cn.LeetCode.第二周.排序;

import cn.LeetCode.第一周.链表.ListNode;

public class 合并k个升序链表23 {
    //这是我自己相出的办法，虽然过了，但时间和空间有点拉胯，需要改进
    public ListNode mergeKLists(ListNode[] lists) {
        if(lists==null||lists.length==0){
            return null;
        }
        ListNode head=lists[0];
        if(head==null&&lists.length==1){
            return head;
        }
        for(int i=0;i<lists.length-1;i++){
            head=hebing(head,lists[i+1]);
        }
        return head;
    }
    public ListNode hebing(ListNode h1,ListNode h2){
        if(h1==null){
            return h2;
        }
        if(h2==null){
            return h1;
        }
        if(h1.val<h2.val){
            h1.next=hebing(h1.next,h2);
            return h1;

        }
        else{
            h2.next=hebing(h1,h2.next);
            return h2;
        }
    }
    //方法二，分而治之，每次合并k/2个链表
    public ListNode mergeKLists2(ListNode[] lists) {
        return merge(lists, 0, lists.length - 1);
    }

    public ListNode merge(ListNode[] lists, int l, int r) {
        if (l == r) {
            return lists[l];
        }
        if (l > r) {
            return null;
        }
        int mid = (l + r) >> 1;
        return mergeTwoLists(merge(lists, l, mid), merge(lists, mid + 1, r));
    }
    public ListNode mergeTwoLists(ListNode a, ListNode b) {
        if (a == null || b == null) {
            return a != null ? a : b;
        }
        ListNode head = new ListNode(0);
        ListNode tail = head, aPtr = a, bPtr = b;
        while (aPtr != null && bPtr != null) {
            if (aPtr.val < bPtr.val) {
                tail.next = aPtr;
                aPtr = aPtr.next;
            } else {
                tail.next = bPtr;
                bPtr = bPtr.next;
            }
            tail = tail.next;
        }
        tail.next = (aPtr != null ? aPtr : bPtr);
        return head.next;
    }

}
