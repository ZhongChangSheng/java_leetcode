package cn.LeetCode.第二周.排序;

public class a冒泡排序 {
    public static void main(String[] args) {
        int nums[]={1,3,5,4,8};
        maoPao(nums);
    }
    //比较相邻的元素，前一个比后一个大就交换位置，这样第一轮之后最大的数在最后面
    public static void maoPao(int[] nums){
        int n=nums.length;
        for(int i=0;i<n;i++){
            boolean flag=false;//标志，如果某一趟没有发生交换，则说明未排序区间满足轻在下，重在上，直接跳出循环
            for(int j=0;j<n-1;j++){
                if(nums[j]>nums[j+1]){
                    int tem=nums[j+1];
                    nums[j+1]=nums[j];
                    nums[j]=tem;
                    flag=true;
                }
            }
            if(!flag){
                break;
            }
        }
        for (int num : nums) {
            System.out.print(num+" ");
        }
    }
}
