package cn.LeetCode.第二周.排序;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class 有序数组的平方977 {
    /**
     *  方法一直接平方在进行排序
     */
    public int[] sortedSquares(int[] nums) {
       for(int i=0;i<nums.length;i++){
           nums[i]*=nums[i];
       }
        Arrays.sort(nums);
        return nums;
    }
    /**
     *  方法二使用双指针，找到正数与负数的分界点，负数那边平方后递减，而正数那边平方后递增
     *  利用二路归并排序
     */
    public int[] sortedSquares2(int[] nums) {
        //找到分界点所在位置
        int n=-1;
       for(int i=0;i<nums.length;i++){
           if(nums[i]<0){
               n=i;
           }
           else {
               break;
           }
       }
       //新数组指针
       int index=0;
       int []res=new int[nums.length];
       int i=n,j=i+1;
       while(i>=0||j<nums.length){
           if(i<0){
               res[index]=nums[j]*nums[j];
               ++j;
           }
           else if(j>=nums.length){
               res[index]=nums[i]*nums[i];
               --i;
           }else if(nums[i]*nums[i]<nums[j]*nums[j]){
               res[index]=nums[i]*nums[i];
               --i;
           }else {
               res[index]=nums[j]*nums[j];
               ++j;
           }
           index++;
       }
       return res;
    }
}
