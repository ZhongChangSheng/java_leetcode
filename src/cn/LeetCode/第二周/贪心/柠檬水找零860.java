package cn.LeetCode.第二周.贪心;

import java.util.Arrays;

public class 柠檬水找零860 {
    /**
     * 我这乱写都能过
     * @param bills
     * @return
     */
    public boolean lemonadeChange(int[] bills) {
        int []a=new int[2];
        Arrays.fill(a,0);
        for(int i=0;i<bills.length;i++){
            if(bills[i]==5){
                a[0]++;
            }
            else if(bills[i]==10){
                a[0]--;
                a[1]++;
                if(a[0]<0){
                    return false;
                }
            }else{
                if(a[0]>0&&a[1]>0){
                    a[1]--;
                    a[0]--;
                }else{
                    a[0]=a[0]-3;

                }
                if(a[0]<0){
                    return false;
                }

            }
        }
        if(a[0]<0||a[1]<0){
            return false;
        }
        return true;
    }
}
