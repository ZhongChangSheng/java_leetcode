package cn.LeetCode.第二周.贪心;

public class 摆动序列376 {
    //贪心思想
    //nums = [1,7,4,9,2,5]，add(1),mut(1)
    //如果后一个数大于前一个数 将mut和新的数加到add里面 add(1,7)   add(1,7,4,9)....
    //如果后一个数小于前一个数 将add和新的数加到mut里面 mut(1,7,4) mut(1,7,4,9,2)....
    public int wiggleMaxLength(int[] nums) {
        if (nums.length < 2) {
            return nums.length;
        }
        int add = 1;
        int mut = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] > nums[i - 1]) {
                add = mut + 1;
            } else if (nums[i] < nums[i - 1]) {
                mut = add + 1;
            }
        }
        return Math.max(add, mut);
    }

    /**
     * 法二，动态规划
     * @param nums
     * @return
     */
    public int wiggleMaxLength2(int[] nums) {
        int n = nums.length;
        if (n < 2) {
            return n;
        }
        int[] up = new int[n];
        int[] down = new int[n];
        up[0] = down[0] = 1;
        for (int i = 1; i < n; i++) {
            if (nums[i] > nums[i - 1]) {
                up[i] = Math.max(up[i - 1], down[i - 1] + 1);
                down[i] = down[i - 1];
            } else if (nums[i] < nums[i - 1]) {
                up[i] = up[i - 1];
                down[i] = Math.max(up[i - 1] + 1, down[i - 1]);
            } else {
                up[i] = up[i - 1];
                down[i] = down[i - 1];
            }
        }
        return Math.max(up[n - 1], down[n - 1]);
    }
}