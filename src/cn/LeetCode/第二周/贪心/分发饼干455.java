package cn.LeetCode.第二周.贪心;

import java.util.Arrays;

public class 分发饼干455 {
    public static void main(String[] args) {
        int []g={1,2,3};
        int []s={1,1};
        System.out.println(findContentChildren(g,s));
    }
    public static int findContentChildren(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);
        int count=0;
        int index=0;
        for(int i=0;i<s.length&&index<g.length;i++){
            if(g[index]<=s[i]){
                count++;
                index++;
            }
        }
        return count;
    }
}
