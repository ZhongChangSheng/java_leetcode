package cn.LeetCode.第二周.贪心;

import java.util.Arrays;

public class 最接近三数之和16 {
    public static void main(String[] args) {
        int[] nums={-3,-2,-5,3,-4};
        int target=-1;
        System.out.println(threeSumClosest(nums,target));
    }
    public static int threeSumClosest(int[] nums, int target) {
        int len = nums.length;
        // int res=Integer.MAX_VALUE;不能写
        int res=1000000;
        Arrays.sort(nums);
        if(nums == null || len < 3) return 0;
        for (int i = 0; i < len ; i++) {
            if(i > 0 && nums[i] == nums[i-1]) continue; // 去重，
            int L = i+1;
            int R = len-1;
            while(L < R){
                int sum=nums[i] + nums[L] + nums[R];
                if(sum==target){
                    return target;
                }
                if(Math.abs(sum-target)<Math.abs(res-target)){
                  res=sum;
                }
                if (sum> target) {
                    // 如果和大于 target，移动左指针
                    int r = R - 1;
                    // 移动到下一个不相等的元素
                    while (L < r && nums[r] == nums[R]) {
                        --r;
                    }
                    R = r;
                } else {
                    // 如果和小于 target，移动 右边的指针
                    int l = L + 1;
                    // 移动到下一个不相等的元素
                    while (l< R && nums[l] == nums[L]) {
                        ++l;
                    }
                    L = l;
                }
                }

            }

        return res;
    }


}
