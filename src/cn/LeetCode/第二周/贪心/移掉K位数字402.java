package cn.LeetCode.第二周.贪心;

import java.util.Deque;
import java.util.LinkedList;

public class 移掉K位数字402 {
    //用单调栈，从第一个字符开始遍历
    //如果小于栈底元素，栈底元素出栈，k--,继续和下一个栈底元素比较，直到大于或等于栈底元素且k>0
    public String removeKdigits(String num, int k) {
        Deque<Character> stack = new LinkedList<Character>();
        int len = num.length();
        for (int i = 0; i < len; ++i) {
            char ch = num.charAt(i);
            while (!stack.isEmpty() && k > 0 && stack.peekLast() >ch) {
                stack.pollLast();
                k--;
            }
            stack.offerLast(ch);
        }

        for (int i = 0; i < k; ++i) {
            stack.pollLast();
        }

        StringBuilder sb = new StringBuilder();
        boolean isZero = true;
        while (!stack.isEmpty()) {
            char ch = stack.pollFirst();
            if (isZero && ch == '0') {
                continue;
            }
            isZero = false;
            sb.append(ch);
        }
        return sb.length() == 0 ? "0" :sb.toString();
    }
}
