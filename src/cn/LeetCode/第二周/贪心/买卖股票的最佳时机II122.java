package cn.LeetCode.第二周.贪心;

public class 买卖股票的最佳时机II122 {
    //只要在上坡的起点买入，在山顶卖出，收益就是最大的
    public int maxProfit(int[] prices) {
        int len=prices.length;
        int total=0;
        for(int i=0;i<len-1;i++){
            total+=Math.max(prices[i+1]-prices[i],0);
        }
        return total;
    }
}
