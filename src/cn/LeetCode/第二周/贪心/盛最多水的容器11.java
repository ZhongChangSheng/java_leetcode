package cn.LeetCode.第二周.贪心;

public class 盛最多水的容器11 {
    /**
     * 使用双指针法
     * @param height
     * @return
     */
    public int maxArea(int[] height) {
        int l = 0, r = height.length - 1;
        int res = 0;
        while (l < r) {
            int area = Math.min(height[l], height[r]) * r;
            res = Math.max(res, area);
            if (height[l] <= height[r]) {
                ++l;
            }
            else {
                --r;
            }
        }
        return res;
        }
}
