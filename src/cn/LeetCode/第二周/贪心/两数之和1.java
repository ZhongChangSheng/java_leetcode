package cn.LeetCode.第二周.贪心;

import java.util.HashMap;
import java.util.Map;

public class 两数之和1 {
    public int[] twoSum(int[] nums, int target) {
        int[] sum=new int[2];
        for(int i=0;i<nums.length-1;i++){
            for(int j=i+1;j<nums.length;j++){
                if(nums[i]+nums[j]==target){
                    sum[0]=i;
                    sum[1]=j;
                }
            }
        }
        return sum;
    }
    public int[] twoSum2(int[] nums, int target) {
        Map<Integer,Integer> map=new HashMap<>();
        int[]sum=new int[2];
        for(int i=0;i<nums.length;i++){
            if(map.containsKey(target-nums[i])){
                sum[0]=map.get(target-nums[i]);
                sum[1]=i;
            }
            else{
                map.put(nums[i],i);
            }
        }
        return sum;
    }
}
