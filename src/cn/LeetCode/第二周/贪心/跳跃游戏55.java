package cn.LeetCode.第二周.贪心;

public class 跳跃游戏55 {
    public boolean canJump(int[] nums) {
        //从最后一个数遍历
        //如果前一个数不能跳跃到，指针往前移1位，继续判断
        //如果前一个数能跳到，则最后一个数移到能跳跃到的数，判断
        if(nums.length==1){
            return true;
        }
        int R=nums.length-1;
        int L=R-1;
        while(L>=0&&L<R){
            if(nums[L]>=(R-L)){
                //如果前一个数能跳到，则最后一个数移到能跳跃到的数
                R=L;
                //L往左移
                --L;
                if(L==-1){
                    return true;
                }
            }
            else{
                //如果前一个数不能跳跃到，指针往前左1位，继续判断
                --L;
            }
        }
        return false;
    }
}
