package cn.LeetCode.第二周.贪心;

public class 加油站134 {
    //方法一，能运行，但超时了，不需要全部遍历一遍，会超时
    public int canCompleteCircuit1(int[] gas, int[] cost) {
        int len=gas.length;
        for(int i=0;i<len;i++){
            int count=0;
            int v=gas[i];
            int start=i;
            while(count<=len){

                if(start+count<len){
                    if(v-cost[start+count]<0){
                        break;
                    }
                    if(start+count==len-1){
                        v=v-cost[start+count]+gas[start+count+1-len];
                    }else{
                        v=v-cost[start+count]+gas[start+count+1];
                    }
                }else{
                    if(v-cost[start+count-len]<0){
                        break;
                    }
                    v=v-cost[start+count-len]+gas[start+count+1-len];
                }
                if(v<0){
                    break;
                }
                ++count;
                if(count==len){
                    return i;
                }
            }

        }
        return -1;
    }
    //改进后，如果从0到第n+1个加油站不行，那么0~n之间的任意一个加油站都到不了n+1
    public int canCompleteCircuit(int[] gas, int[] cost) {
        int len = gas.length;
        int i = 0;
        while (i < len) {
            int sum1 = 0, sum2 = 0;
            int count = 0;
            while (count < len) {
                int n = (i + count) % len;
                sum1 += gas[n];
                sum2 += cost[n];
                if (sum1 > sum2) {
                    break;
                }
                count++;
            }
            if (count == len) {
                return i;
            } else {
                i = i + count + 1;
            }
        }
        return -1;
    }
}
