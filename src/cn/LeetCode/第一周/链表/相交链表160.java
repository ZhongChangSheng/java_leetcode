package cn.LeetCode.第一周.链表;

import java.util.HashSet;
import java.util.Set;

public class 相交链表160 {
    //双指针法，两个链表相交，那么相交点之后的长度是相同的
    //pA走完自己的路程之后跳到B,pb走完自己的路程后跳到A,最终以链表较短的那条作为起点
    //a=8,b=5,c=4(公共长度) b走完之后跳到a,此时a距离终点3，a到大终点跳到b,b走到了3的位置，此时连个指针起点齐平
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        //如果headA,headB一个为空或者两个为空，说明没有交点，返回null
        if(headA==null||headB==null){
            return null;
        }
        //定义两个指针
        ListNode pA=headA;
        ListNode pB=headB;
        //两个指针不相等，则继续遍历
        while(pA!=pB){
            if(pA==null){
                pA=headB;
            }else{
                pA=pA.next;
            }

            if(pB==null){
                pB=headA;
            }else{
                pB=pB.next;
            }
        }
        return pA;
    }
    //用set集合来做，存入headA,在遍历headB,让headB的每一个结点在set里面比较
    public ListNode getIntersectionNode2(ListNode headA, ListNode headB) {
        Set<ListNode> visited = new HashSet<ListNode>();
        ListNode temp = headA;
        while (temp != null) {
            visited.add(temp);
            temp = temp.next;
        }
        temp = headB;
        while (temp != null) {
            if (visited.contains(temp)) {
                return temp;
            }
            temp = temp.next;
        }
        return null;
    }

}
