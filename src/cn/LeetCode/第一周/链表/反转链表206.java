package cn.LeetCode.第一周.链表;

/**
 * 1->2->3->null
 *
 *
 */
public class 反转链表206 {
    //遍历链表，直接改变指针的指向null<-1<-2<-3,时间复杂度为O(n)
    public ListNode reverseList(ListNode head) {
        ListNode pre = null;
        ListNode current = head;
        while (current != null) {
            //记录当前结点的下一个结点
            ListNode next = current.next;
            //令当前结点的指针指向上一个节点
            current.next = pre;
            //将上一个节点变为当前结点
            pre = current;
            //将当前节点变为下一个结点
            current = next;
        }
        return pre;
    }
    //递归的方法，nk->nk+1,要让nk<-nk+1,
    // 也就是nk+1.next=nk,而nk+1=nk.next,所以nk.next.next=nk;
    public ListNode reverseList2(ListNode head) {
       if(head==null||head.next==null){
           return head;
       }
       ListNode newHead=reverseList2(head.next);
       head.next.next=head;
       head.next=null;
       return newHead;
    }
}
