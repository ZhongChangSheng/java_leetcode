package cn.LeetCode.第一周.链表;

import java.util.HashSet;
import java.util.Set;

public class 环形链表II142 {
    public ListNode detectCycle(ListNode head) {
        ListNode pre=head;
        Set<ListNode> visit=new HashSet<>();
        while(pre!=null){
            //利用contains方法判断Set里面是否有这个节点
            if(visit.contains(pre)){
                //有，直接返回
                return pre;
            }else{
                //无，添加到set里面
                visit.add(pre);
            }
            pre=pre.next;
        }
        return null;
    }
    //双指针法（快慢指针）最终两指针会在循环结点相遇
    public ListNode detectCycle2(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode slow = head, fast = head;
        while (fast != null) {
            slow = slow.next;
            if (fast.next != null) {
                fast = fast.next.next;
            } else {
                return null;
            }
            if (fast == slow) {
                ListNode ptr = head;
                while (ptr != slow) {
                    ptr = ptr.next;
                    slow = slow.next;
                }
                return ptr;
            }
        }
        return null;

    }


}
