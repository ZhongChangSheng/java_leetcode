package cn.LeetCode.第一周.链表;

import java.util.HashMap;
import java.util.Map;

/**
 * 浅拷贝： 返回地址一样的链表。 深拷贝： 返回地址不一样，但关系一致的链表。
 * 所以不能直接简单粗暴的遍历复制， 这样复制出来的是一样的地址，不符合题目的深拷贝要求。
 */
public class 复制带随机指针的链表138 {
    Map<Node,Node> createNode=new HashMap<Node,Node>();
    public Node copyRandomList(Node head) {
        if(head==null){
            return null;
        }
        if(!createNode.containsKey(head)){
            Node newHead=new Node(head.val);
            createNode.put(head,newHead);
            newHead.next=copyRandomList(head.next);
            newHead.random=copyRandomList(head.random);
        }
        return createNode.get(head) ;
    }
    public Node copyRandomList2(Node head){
        //在链表原有基础上每一个结点后面复制一个节点
        for(Node p=head;p!=null;p=p.next.next){
            Node q=new Node(p.val);
            q.next=p.next;
            p.next=q;
        }
        //为复制的结点添加random指针
        for(Node p=head;p!=null;p=p.next.next){
           if(p.random!=null){
               p.next.random=p.random.next;
           }
        }
        //拆分两个链表，并复原原链表
        Node dummy = new Node(-1), cur = dummy;
        for(Node p = head; p != null; p = p.next)
        {
            Node q = p.next;
            cur = cur.next = q;
            p.next = q.next;
        }

        return dummy.next;
    }


}
