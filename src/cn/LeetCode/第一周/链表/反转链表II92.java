package cn.LeetCode.第一周.链表;

/**
 * 1->2->3->4->5->6
 * left=3,right=5
 */
public class 反转链表II92 {
    public ListNode reverseBetween(ListNode head, int left, int right) {
        // 因为头节点有可能发生变化，使用虚拟头节点可以避免复杂的分类讨论
        ListNode dummyNode = new ListNode(-1);
        dummyNode.next = head;

        ListNode pre = dummyNode;
        // 第 1 步：从虚拟头节点走 left - 1 步，来到 left 节点的前一个节点
        // 建议写在 for 循环里，语义清晰
        for (int i = 1; i < left; i++) {
            pre = pre.next;//最终pre停在2
        }

        // 第 2 步：从 pre也就是2 再走 right - left + 2 步，来到 right 节点
        ListNode rightNode = pre;
        for (int i = 1; i < right - left + 2; i++) {
            rightNode = rightNode.next;//rightNode停在5
        }

        // 第 3 步：切断出一个子链表（截取链表）
        ListNode leftNode = pre.next;//3->4->5->6
        ListNode curr = rightNode.next;//6

        // 注意：切断链接
        pre.next = null;//pre:1->2
        rightNode.next = null;//leftNode:3->4->5

        // 第 4 步：同第 206 题，反转链表的子区间
        reverse(leftNode);//leftNode:3<-4<-5

        // 第 5 步：接回到原来的链表中
        pre.next = rightNode;//1->2->5->4->3
        leftNode.next = curr;//1->2->5->4->3->6
        return dummyNode.next;
    }
    public ListNode reverse(ListNode head){
        if(head==null||head.next==null){
            return head;
        }
        ListNode newHead=reverse(head.next);
        head.next.next=head;
        head.next=null;
        return newHead;
    }
}
