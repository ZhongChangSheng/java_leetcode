package cn.LeetCode.第一周.栈;

import java.util.Deque;
import java.util.LinkedList;
/**
 * 用两个栈来辅助，每次存入栈中时先peek()比较大小，小的存入minStack,每次stack出栈，minStack也跟着出栈
 */

public class 最小栈155 {
    Deque<Integer> stack;
    Deque<Integer> minStack;
    public 最小栈155() {
        stack=new LinkedList<Integer>();
        minStack=new LinkedList<Integer>();
        minStack.push(Integer.MAX_VALUE);
    }

    public void push(int val) {
        stack.push(val);
        minStack.push(Math.min(minStack.peek(),val));
    }

    public void pop() {
        stack.pop();
        minStack.pop();
    }

    public int top() {
        return stack.peek();
    }

    public int getMin() {
        return minStack.peek();
    }
}
