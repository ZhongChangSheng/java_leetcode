package cn.LeetCode.第一周.栈;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

public class 每日温度739 {
    //第一种方法，没有用栈来做
    public int[] dailyTemperatures(int[] temperatures) {
        int []a=new int[temperatures.length];
        for(int i=0;i<temperatures.length;i++){
            a[i]=dfs(temperatures,i+1,temperatures[i]);
        }
        return a;
    }

    public int dfs(int []b,int k,int m){
        int count=1;
        for(int i=k;i<b.length;i++){
            if(m<b[i]){
                return count;
            }
            count++;
        }
        return 0;

    }
    //递减栈
    public int[] dailyTemperatures2(int[] temperatures) {
        Deque<Integer> stack=new LinkedList<Integer>();
        int[] res=new int[temperatures.length];
        stack.push(0);
        for(int i=1;i<temperatures.length;i++){
            while(!stack.isEmpty()&&temperatures[stack.peek()]<temperatures[i]){
                res[stack.peek()]=i-stack.peek();
                stack.pop();
            }
            stack.push(i);

        }
        return res;
    }
}
