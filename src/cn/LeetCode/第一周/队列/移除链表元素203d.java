package cn.LeetCode.第一周.队列;

import cn.LeetCode.第一周.链表.ListNode;

public class 移除链表元素203d {
    //递归
    public ListNode removeElements(ListNode head, int val) {
        if(head==null){
            return head;
        }
        head.next=removeElements(head.next,val);
        if(head.val==val){
            return head.next;
        }
        else{
            return head;
        }
    }
    //迭代
    public ListNode removeElements2(ListNode head, int val) {
      ListNode dumpNode=new ListNode(0);
      dumpNode.next=head;
      ListNode tem=dumpNode;
      while (tem.next!=null){
          if(tem.next.val==val){
              tem.next=tem.next.next;
          }else {
              tem=tem.next;
          }
      }
      return dumpNode.next;
    }
}
