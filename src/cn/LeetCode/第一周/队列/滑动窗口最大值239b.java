package cn.LeetCode.第一周.队列;

import java.util.ArrayDeque;
import java.util.Deque;

public class 滑动窗口最大值239b {
    //这个会超时
    public int[] maxSlidingWindow(int[] nums, int k) {
        int len=nums.length-k+1;
        int[] n=new int[len];
        int max=Integer.MIN_VALUE;
        for(int i=0;i<=nums.length-k;i++){
            for(int j=i;j<i+k;j++){
                max=Math.max(max,nums[j]);
            }
            n[i]=max;
            max=0;
        }
        return n;
    }
    //用队列来实现
    //初始时单调队列为空，随着对数组的遍历过程中，每次插入元素前，需要考察两个事情：

            //1、合法性检查：队头下标如果距离 i 超过了 k ，则应该出队。
          //  2、单调性维护：如果 nums[i] 大于或等于队尾元素下标所对应的值，
    //  则当前队尾再也不可能充当某个滑动窗口的最大值了，故需要队尾出队，
    //  始终保持队中元素从队头到队尾单调递减。
           // 3、如次遍历一遍数组，队头就是每个滑动窗口的最大值所在下标。

    public int[] maxSlidingWindow2(int[] nums, int k) {
        int n = nums.length;
        Deque<Integer> queue = new ArrayDeque<>(); //双端队列
        int[] res = new int[n - k + 1];
        for (int i = 0 , j = 0; i < n; i++) {
            //判断队头是否在滑动窗口范围内
            while (!queue.isEmpty() && i- k + 1 > queue.getFirst())
                queue.pollFirst();//pollFirst()方法用于返回此双端队列表示的队列的第一个元素，但会从此双端队列中删除第一个元素。
            //维护单调递减队列
            while (!queue.isEmpty() && nums[i] > nums[queue.getLast()])
                queue.pollLast();
            queue.offer(i);    //将当前元素插入队尾
            //滑动窗口的元素达到了k个，才可以将其加入答案数组中
            if( i - k + 1 >= 0) res[j++] = nums[queue.getFirst()];
        }
        return res;
    }

}
