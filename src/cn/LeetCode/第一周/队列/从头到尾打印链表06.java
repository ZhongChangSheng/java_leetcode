package cn.LeetCode.第一周.队列;

import cn.LeetCode.第一周.链表.ListNode;

import java.util.Deque;
import java.util.LinkedList;
//输入一个链表的头节点，从尾到头反过来返回每个节点的值（用数组返回）。
public class 从头到尾打印链表06 {
    public int[] reversePrint(ListNode head) {
        Deque<Integer> stack=new LinkedList<Integer>();
        int count=0;
        while(head!=null){
            stack.push(head.val);
            count++;
            head=head.next;
        }
        int[] n=new int[count];
        for(int i=0;i<count;i++){
            n[i]=stack.pop();
        }
        return n;

    }
}
