package cn.LeetCode.第一周.队列;

import cn.LeetCode.第一周.链表.ListNode;

public class 奇偶链表328g {
    public ListNode oddEvenList(ListNode head) {
        if(head==null){
            return head;
        }
        ListNode evenHead=head.next;
        ListNode old=head;
        ListNode even=evenHead;
        while(even!=null&&even.next!=null){
            old.next=even.next;
            old=old.next;
            even.next=old.next;
            even=even.next;
        }
        old.next=evenHead;
        return head;
    }
}
