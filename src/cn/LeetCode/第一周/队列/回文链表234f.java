package cn.LeetCode.第一周.队列;

import cn.LeetCode.第一周.链表.ListNode;

import java.util.ArrayList;
import java.util.List;

public class 回文链表234f {
    //双指针法，将链表添加到集合，通过前后指针不断比较
    public boolean isPalindrome(ListNode head) {
        ListNode newHead=head;
        List<Integer> list=new ArrayList<>();
        while(newHead!=null){
            list.add(newHead.val);
            newHead=newHead.next;
        }
        int front=0;
        int last=list.size()-1;
        while(front<last){
            if(list.get(front)!=list.get(last)){
                return false;
            }
            front++;
            last--;
        }
        return true;
    }
}
