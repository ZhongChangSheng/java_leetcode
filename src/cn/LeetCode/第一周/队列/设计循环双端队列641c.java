package cn.LeetCode.第一周.队列;

public class 设计循环双端队列641c {
        int[] queue;
        int front, rear;
        int k;
        int size;
        public 设计循环双端队列641c(int k) {
            queue = new int[k];
            front = 0;
            rear = 0;
            this.k = k;
            size = 0;
        }

        //	将一个元素添加到双端队列头部。 如果操作成功返回 true ，否则返回 false
        public boolean insertFront(int value) {
            if (size == k) {
                return false;
            } else {
                queue[front] = value;
                front--;
                front = (front + k) % k;
                size++;
                return true;
            }
        }

        //	将一个元素添加到双端队列尾部。如果操作成功返回 true ，否则返回 false 。
        public boolean insertLast(int value) {
            if (size == k) {
                return false;
            } else {
                rear++;
                rear %= k;
                queue[rear] = value;
                size++;
                return true;
            }
        }

        //	从双端队列头部删除一个元素。 如果操作成功返回 true ，否则返回 false
        public boolean deleteFront() {
            if (size == 0) {
                return false;
            } else {
                front++;
                front %= k;
                size--;
                return true;
            }
        }

        //	从双端队列尾部删除一个元素。如果操作成功返回 true ，否则返回 false 。
        public boolean deleteLast() {
            if (size == 0) {
                return false;
            } else {
                rear--;
                rear = (rear + k) % k;
                size--;
                return true;
            }
        }

        //	从双端队列头部获得一个元素。如果双端队列为空，返回 -1 。
        public int getFront() {
            if (size == 0) {
                return -1;
            } else {
                return queue[(front + 1) % k];
            }
        }

        public int getRear() {
            if (size == 0) {
                return -1;
            } else {
                return queue[rear];
            }
        }

        public boolean isEmpty() {
            return size == 0;
        }

        public boolean isFull() {
            return size == k;
        }
    }


