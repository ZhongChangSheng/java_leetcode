package cn.LeetCode.第一周.队列;

import cn.LeetCode.第一周.链表.ListNode;

public class 链表中倒数第K个节点22 {
    //顺序遍历，找到链表的长度
    public ListNode getKthFromEnd(ListNode head, int k) {
        ListNode newhead=null;
        int n=0;
        for(newhead=head;newhead!=null;newhead=newhead.next){
            n++;
        }
        for(int i=0;i<n-k;i++){
            head=head.next;
        }
        return head;
    }
    //快慢指针
    public ListNode getKthFromEnd2(ListNode head, int k) {
        ListNode fast=head;
        ListNode slow=head;
        while (fast!=null&&k>0){
            fast=fast.next;
            k--;
        }
        while (fast!=null){
            fast=fast.next;
            slow=slow.next;
        }
        return slow;

    }
}
