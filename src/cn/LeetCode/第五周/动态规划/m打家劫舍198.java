package cn.LeetCode.第五周.动态规划;

public class m打家劫舍198 {
    //dp[i][0]代表不偷，dp[i][1]代表偷
    public int rob(int[] nums) {
        int n=nums.length;
        int[] []dp=new int[n][2];
        dp[0][0]=0;
        dp[0][1]=nums[0];
        for(int i=1;i<n;i++){
            //这一天不偷的最大值
            dp[i][0]=Math.max(dp[i-1][0],dp[i-1][1]);
            //这一天偷了+昨天不偷的最大值=这一天偷了的最大值
            dp[i][1]=dp[i-1][0]+nums[i];
        }
        //返回最后一天偷或不偷的最大值
        return Math.max(dp[n-1][0],dp[n-1][1]);
    }
}
