package cn.LeetCode.第五周.动态规划;

public class i买卖股票的最佳时机122 {
    //一，动态规划
    public int maxProfit(int[] prices) {
        int n = prices.length;
        int[][] dp = new int[n][2];
        dp[0][0] = 0;
        dp[0][1] = -prices[0];
        for (int i = 1; i < n; i++) {
            dp[i][0] = Math.max(dp[i - 1][0], dp[i - 1][1] + prices[i]);
            dp[i][1] = Math.max(dp[i - 1][1], dp[i - 1][0] - prices[i]);
        }
        return dp[n - 1][0];
    }
    //二，改版
    public int maxProfit2(int[] prices) {
        int n = prices.length;
       int total=0;
        for (int i = 1; i < n; ++i) {
           total+=Math.max(prices[i]-prices[i-1],0);
        }
        return total;
    }
}
