package cn.LeetCode.第五周.动态规划;

public class b斐波那契数509 {
    public int fib(int n) {
        if(n==0||n==1){
            return n;
        }
        int f=0,f1=0,f2=1;
        for(int i=2;i<=n;i++){
            f=f1+f2;
            f1=f2;
            f2=f;
        }
        return f;
    }
}
