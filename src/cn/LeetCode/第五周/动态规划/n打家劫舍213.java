package cn.LeetCode.第五周.动态规划;

public class n打家劫舍213 {
    public static void main(String[] args) {
        int []nums={2,3,2};
        System.out.println(rob(nums));
    }
    public static int rob(int[] nums) {
        int n=nums.length;
        //dp[i][0]表示不偷时的最大值
        //dp[i][1]表示偷时的最大值
        //偷了第一家，最后一家就不能偷0-n-2
        //第一家不偷，最后一家能偷 1-n-1;
        int[][]dp=new int[n][2];
        dp[1][0]=nums[0];
        dp[1][1]=nums[0];
        for(int i=2;i<n-1;i++){
            dp[i][0]=Math.max(dp[i-1][0],dp[i-1][1]);
            dp[i][1]=dp[i-1][0]+nums[i];
        }
        int a=Math.max(dp[n-2][0],dp[n-2][1]);
        System.out.println(a);
        dp[1][0]=0;
        dp[1][1]=nums[1];
        for(int i=2;i<n;i++){
            dp[i][0]=Math.max(dp[i-1][0],dp[i-1][1]);
            dp[i][1]=dp[i-1][0]+nums[i];
        }
        int b=Math.max(dp[n-1][0],dp[n-1][1]);
        System.out.println(b);
        return a>b?a:b;
    }
}
