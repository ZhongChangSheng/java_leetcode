package cn.LeetCode.第五周.动态规划;

public class c最大子序和53 {
    public int maxSubArray(int[] nums) {
        int len=nums.length;
        int sum=nums[0];
        int maxSum=sum;
        for(int i=1;i<len;i++){
            sum=Math.max(sum,0)+nums[i];
            maxSum=Math.max(sum,maxSum);

        }
        return maxSum;

    }
}
