package cn.LeetCode.第五周.动态规划;

public class l整数拆分343 {
    //n拆成K和n-k
    //dp[i]表示i拆成正整数的最大值
    //i(j,i-j(i-j不再拆分)) i*(i-j)
    //i(j,i-j(i-j继续拆分)) j*dp[i-j]
    public int integerBreak(int n) {
        int[]dp=new int[n+1];
        dp[0]=0;
        dp[1]=0;
        for(int i=2;i<=n;i++){
            dp[i]=0;
            for(int j=1;j<i;j++){
                dp[i]=Math.max(Math.max(j*(i-j),j*dp[i-j]),dp[i]);
            }
        }
        return dp[n];
    }
}
