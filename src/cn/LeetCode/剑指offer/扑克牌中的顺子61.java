package cn.LeetCode.剑指offer;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class 扑克牌中的顺子61 {
    public static void main(String[] args) {
        int[] nums={0,0,2,2,5};
        System.out.println(isStraight(nums));
    }
    public static boolean isStraight(int[] nums) {
        Arrays.sort(nums);
        int count=0;
        for(int i=0;i<nums.length-1;i++){
            if(nums[i]==0){
                count++;
            }else{
                if(nums[i+1]-nums[i]>count+1){
                    return false;
                }
               if(nums[i+1]==nums[i]){
                   return false;
               }

            }

        }
        return true;
    }
    //方法二
    public boolean isStraight2(int[] nums) {
        Set<Integer> repeat = new HashSet<>();
        int max = 0, min = 14;
        for(int num : nums) {
            if(num == 0) continue; // 跳过大小王
            max = Math.max(max, num); // 最大牌
            min = Math.min(min, num); // 最小牌
            if(repeat.contains(num)) return false; // 若有重复，提前返回 false
            repeat.add(num); // 添加此牌至 Set
        }
        return max - min < 5; // 最大牌 - 最小牌 < 5 则可构成顺子
    }

}
