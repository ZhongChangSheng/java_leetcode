package cn.LeetCode.剑指offer;

public class t连续子数组的最大和42 {
    //动态规划
    //dp[i]表示以i结尾的子数组最大值dp[i]=Math.max(dp[i-1]+nums[i],nums[i]);
    //maxSum表示子数组最大值maxSum=Math.max(maxSum,dp[i])
    public int maxSubArray(int[] nums) {
//        int n=nums.length;
//        int[]dp=new int[n];
//        dp[0]=nums[0];
//        int maxSum=dp[0];
//        for(int i=1;i<n;i++){
//            dp[i]=Math.max(dp[i-1]+nums[i],nums[i]);
//            maxSum=Math.max(dp[i],maxSum);
//        }
//        return maxSum;
        int n=nums.length;
        int sum=nums[0];
        int maxSum=sum;
        for(int i=1;i<n;i++){
            sum=Math.max(sum,0)+nums[i];
            maxSum=Math.max(sum,maxSum);
        }
        return maxSum;
    }
}
