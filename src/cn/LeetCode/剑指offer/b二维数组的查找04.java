package cn.LeetCode.剑指offer;

public class b二维数组的查找04 {
    public boolean findNumberIn2DArray(int[][] matrix, int target) {
        if(matrix==null||matrix.length==0||matrix[0].length==0){
            return false;
        }
        int rows = matrix.length, colums = matrix[0].length;
        int row=0,colum=colums-1;
        while (row < rows&&colum>=0) {
            if(matrix[row][colum]>target){
                colum--;
            }else if(matrix[row][colum]<target){
                row++;
            }else{
                return true;
            }
        }
        return false;
    }
}
