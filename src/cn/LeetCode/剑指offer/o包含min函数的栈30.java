package cn.LeetCode.剑指offer;

import java.util.Deque;
import java.util.LinkedList;

public class o包含min函数的栈30 {
    Deque<Integer> stack;//记录最后放入的数据，也就是最顶部
    Deque<Integer> minStack;//记录最小的数
    public o包含min函数的栈30() {
        stack = new LinkedList<Integer>();
        minStack = new LinkedList<Integer>();
        minStack.push(Integer.MAX_VALUE);
    }

    public void push(int x) {
        stack.push(x);
        minStack.push(Math.min(minStack.peek(),x));
    }

    public void pop() {
        stack.pop();
        minStack.pop();
    }

    public int top() {
        return stack.peek();
    }

    public int min() {
        return minStack.peek();
    }
}
