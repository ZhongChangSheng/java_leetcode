package cn.LeetCode.剑指offer;

public class g矩阵中的路径12 {
    public boolean exist(char[][] board, String word) {
        int m=board.length;
        int n=board[0].length;
        boolean[][]visited=new boolean[m][n];
        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
                if(dfs(board,word,0,i,j,visited)) {
                    return true;
                }
            }
        }
        return false;
    }
    public boolean dfs(char[][]board,String str,int k,int i,int j,boolean[][]visited){
        //递归出口
        if(board[i][j]!=str.charAt(k)){
            return false;
        }else if(k==str.length()-1){
            return true;
        }
        visited[i][j]=true;
        boolean result=false;
        //上下左右移动
        int [][]direction={{0,1},{0,-1},{1,0},{-1,0}};
        for(int[]dir:direction){
            int row=i+dir[0];
            int cow=j+dir[1];
            if(row>=0&&row<board.length&&cow>=0&&cow<board[0].length){
                if(!visited[row][cow]){
                    if(dfs(board,str,k+1,row,cow,visited)){
                        result=true;
                        break;
                    }
                }
            }
        }
        visited[i][j]=false;
        return result;

    }
}
