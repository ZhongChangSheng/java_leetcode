package cn.LeetCode.剑指offer;

public class i调整数组顺序使奇数2位于偶数前边21 {
    public int[] exchange(int[] nums) {
        int start = 0;
        int end = nums.length - 1;
        while (start < end) {
            if (nums[start] % 2 == 1 && nums[end] % 2 == 0) {
                start++;
                end--;
            } else if (nums[start] % 2 == 1 && nums[end] % 2 == 1) {
                start++;
            } else if (nums[start] % 2 == 0 && nums[end] % 2 == 1) {
                int tem = nums[end];
                nums[end--] = nums[start];
                nums[start++] = tem;

            } else {
                end--;
            }
        }
        return nums;
    }
}
