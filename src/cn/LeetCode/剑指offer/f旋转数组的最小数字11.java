package cn.LeetCode.剑指offer;

public class f旋转数组的最小数字11 {
    public int minArray(int[] numbers) {
        int l = 0;
        int r = numbers.length-1;
        int mid = (l+r)/2;
        //本就是有序数组
        if(numbers[r] > numbers[l]) return numbers[l];

        while(l <= r){
            //如果二分后的数组是有序数组，则返回最左元素，即为最小
            if(numbers[r] > numbers[l]) return numbers[l];
            //若最左小于mid元素，则最左到mid是严格递增的，那么最小元素必定在mid之后
            if(numbers[l] < numbers[mid]){
                l = mid+1;
                mid = (l+r)/2;
            }
            //若最左大于mid元素，则最小元素必定在最左到mid之间(不包括最左，因为最左已经大于mid)
            else if(numbers[l] > numbers[mid]){
                r = mid;
                l++;
                mid = (l+r)/2;
            }
            //若二者相等，则最小元素必定在l+1到r之间，因为l和mid相等，故可以去除
            else{
                l++;
                mid = (l+r)/2;
            }
        }
        return numbers[mid];
    }
    public int minArray2(int[] numbers) {
        int len=numbers.length;
        if(len==1){
            return numbers[0];
        }
        for(int i=1;i<len;i++){
            if(numbers[i]<numbers[i-1]){
                return numbers[i];
            }
        }
        return numbers[0];
    }
}
