package cn.LeetCode.剑指offer;

public class 和为s的两个数字57 {
    public int[] twoSum(int[] nums, int target) {
        int n=nums.length;
        int[] res=new int[2];
        int i=0,j=n-1;
        while(i<j){
            int sum=nums[i]+nums[j];
            if(sum<target){
                i++;
            }else if(sum>target){
                j--;
            }
            else{
                res[0]=nums[i];
                res[1]=nums[j];
                return res;
            }
        }
        return res;

    }
}
