package cn.LeetCode.剑指offer;

import cn.LeetCode.第一周.链表.ListNode;

public class j链表中倒数第k个节点22 {
    public ListNode getKthFromEnd(ListNode head, int k) {
        int count=0;
        ListNode node=head;
        while(node!=null){
            count++;
            node=node.next;
        }
        for(int i=1;i<=(count-k);i++){
            head=head.next;
        }
        return head;
    }
    //法二，快慢指针
    public ListNode getKthFromEnd2(ListNode head, int k) {

        ListNode fast=head;
        ListNode slow=head;
        while(fast!=null&&k>0){
            fast=fast.next;
            k--;
        }
        while(fast!=null){
            slow=slow.next;
            fast=fast.next;
        }
        return slow;
    }
}
