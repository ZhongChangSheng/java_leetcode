package cn.LeetCode.剑指offer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class p从上到下打印二叉树32 {
    public int[] levelOrder(TreeNode root) {
        if(root==null){
            return new int [0];
        }
        Queue<TreeNode> que=new LinkedList<TreeNode>();
        List<Integer> list=new ArrayList<>();
        que.add(root);
        while(!que.isEmpty()){
            TreeNode node=que.poll();
            list.add(node.val);
            if(node.left!=null){
                que.add(node.left);
            }
            if(node.right!=null){
                que.add(node.right);
            }

        }
        int[]res=new int[list.size()];
        for(int i=0;i<list.size();i++){
            res[i]=list.get(i);
        }
        return res;

    }
}
