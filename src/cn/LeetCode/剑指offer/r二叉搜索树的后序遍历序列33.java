package cn.LeetCode.剑指offer;

import java.util.Stack;

public class r二叉搜索树的后序遍历序列33 {
    public boolean verifyPostorder(int[] postorder) {
        Stack<Integer> deque=new Stack<>();
        int root=Integer.MAX_VALUE;
        deque.add(root);
        for(int i=postorder.length-1;i>=0;i--){
            if(postorder[i]>root){
                return false;
            }
            while(!deque.isEmpty()&&deque.peek()>postorder[i])
                root=deque.pop();
            deque.add(postorder[i]);
        }
        return true;
    }
}
