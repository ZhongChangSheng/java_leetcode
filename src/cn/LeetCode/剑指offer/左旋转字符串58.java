package cn.LeetCode.剑指offer;

public class 左旋转字符串58 {
    public String reverseLeftWords(String s, int n) {
        StringBuffer sb=new StringBuffer();
        for(int i=n;i<s.length();i++){
            sb.append(s.charAt(i));
        }
        for(int i=0;i<n;i++){
            sb.append(s.charAt(i));
        }
        return sb.toString();

    }
    public String reverseLeftWords2(String s, int n) {
        return s.substring(n, s.length()) + s.substring(0, n);

    }
}
