package cn.LeetCode.剑指offer;

import java.util.Arrays;

public class 构建乘积数组66 {
    public int[] constructArr(int[] a) {
        int len=a.length;
        if(len==0){
            return  a;
        }
        //数左边的乘积乘右边的乘积
        int[]b=new int[len];
        //数左边的乘积
        int[]left=new int[len];
        //数右边的乘积
        int[]right=new int[len];
        left[0]=1;
        for(int i=1;i<len;i++){
            left[i]=a[i-1]*left[i-1];
        }
        right[len-1]=1;
        for(int i=len-2;i>=0;i--){
            right[i]=right[i+1]*a[i+1];
        }
        for(int i=0;i<len;i++){
            b[i]=left[i]*right[i];
        }
        return b;
    }
}
