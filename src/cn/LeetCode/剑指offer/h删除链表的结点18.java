package cn.LeetCode.剑指offer;

import cn.LeetCode.第一周.链表.ListNode;

public class h删除链表的结点18 {
    public ListNode deleteNode(ListNode head, int val) {
        //若刚好删除的结点是头结点
        if(head.val==val){
            return head.next;
        }
        ListNode pre=head,cur=head.next;
        while(cur!=null){
            if(cur.val==val){
                pre.next=cur.next;
                break;
            }
            else{
                pre=cur;
                cur=cur.next;
            }

        }
        return head;
    }
}
