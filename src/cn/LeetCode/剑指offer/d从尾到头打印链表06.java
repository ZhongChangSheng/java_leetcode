package cn.LeetCode.剑指offer;

import cn.LeetCode.第一周.链表.ListNode;

import java.util.Deque;
import java.util.LinkedList;

public class d从尾到头打印链表06 {
    //方法一使用栈来存放head的值,时间复杂度On,空间复杂度On
    public int[] reversePrint(ListNode head) {
        Deque<Integer> deque=new LinkedList<>();
        while(head!=null){
            deque.push(head.val);
            head=head.next;
        }
        int len=deque.size();
        int[]n=new int[len];
        for(int i=0;i<len;i++){
            n[i]=deque.pop();
        }
        return n;
    }
    //方法二，时间复杂度On,空间复杂度O1
    public int[] reversePrint2(ListNode head) {
        ListNode newhead=head;
        int count=0;
        while(newhead!=null){
            count++;
            newhead=newhead.next;
        }
        int[] n=new int[count];
        ListNode chead=head;
        for(int i=count-1;i>=0;i--){
            n[i]=chead.val;
            chead=chead.next;
        }
        return n;
    }
}
