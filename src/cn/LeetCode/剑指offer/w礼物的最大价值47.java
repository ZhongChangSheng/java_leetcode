package cn.LeetCode.剑指offer;

public class w礼物的最大价值47 {
    public int maxValue(int[][] grid) {
        //动态规划
        //dp[i][j]=max(dp[i][j-1]+grid[i][j],dp[i-1][j]+grid[i][j])
        //return dp[m-1][n-1]
        int n=grid[0].length;
        int m=grid.length;
        for(int i=1;i<m;i++){
            grid[i][0]=grid[i-1][0]+grid[i][0];
        }
        for(int j=1;j<n;j++){
            grid[0][j]=grid[0][j-1]+grid[0][j];
        }
        for(int i=1;i<m;i++){
            for(int j=1;j<n;j++){
                grid[i][j]=Math.max(grid[i][j-1]+grid[i][j],grid[i-1][j]+grid[i][j]);
            }

        }

        return grid[m-1][n-1];
    }
}
