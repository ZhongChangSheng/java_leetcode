package cn.LeetCode.剑指offer;

import java.util.Deque;
import java.util.LinkedList;

public class e用两个栈实现队列09 {
    Deque<Integer> insert;
    Deque<Integer> output;
    public e用两个栈实现队列09() {
        insert=new LinkedList<>();
        output=new LinkedList<>();
    }

    public void appendTail(int value) {
        insert.push(value);
    }

    public int deleteHead() {
        if(output.isEmpty()){
            if(insert.isEmpty()){
                return -1;
            }
            while(!insert.isEmpty()){
                output.push(insert.pop());
            }
        }
        return output.pop();

    }
}
