package cn.LeetCode.剑指offer;

import java.util.HashSet;
import java.util.Set;

public class a数组中重复的数字03 {
    public static void main(String[] args) {
        int nums[]={3, 4, 2, 0, 0, 1};
        System.out.println(findRepeatNumber(nums));
    }
    public  static int findRepeatNumber(int[] nums) {
        for(int i=0;i<nums.length-1;i++){
            for(int j=i+1;j<nums.length;j++){
                if(nums[i]==nums[j]){
                    return nums[i];
                }
            }
        }

        return -1;
    }
    public int findRepeatNumber2(int[] nums) {
        Set<Integer> set=new HashSet<>();
        for(int num:nums){
            if(!set.add(num)){
                return num;
            }
        }
        return -1;
    }
}
