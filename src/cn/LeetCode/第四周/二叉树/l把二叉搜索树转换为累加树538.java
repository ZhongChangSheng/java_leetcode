package cn.LeetCode.第四周.二叉树;

public class l把二叉搜索树转换为累加树538 {
    int sum=0;
    //二叉搜索树其实是一个中序遍历，我们对其反遍历，进行累加在复制给结点
    public TreeNode convertBST(TreeNode root) {
        if(root!=null){
            convertBST(root.right);
            sum+=root.val;
            root.val=sum;
            convertBST(root.left);
        }
        return root;
    }
}
