package cn.LeetCode.第四周.二叉树;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 获取前序遍历序列第一个元素A，该元素是二叉树根结点中保存的关键字。
 * 凭借该元素查找中序遍历序列，A左侧的元素（GDBHEI）既是原二叉树中A所在结点左子树保存的关键字，
 * A右侧的元素（FC）既是原二叉树中A所在结点右子树保存的关键字。
 * 对使用A从中序遍历序列中划分出来的左右两部分子序列，执行上述过程，直到所有元素的在原二叉树中的位置确认。
 */
public class f从前序和中序遍历序列构造二叉树105 {
    Map<Integer,Integer> map=new HashMap<>();
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        int len=preorder.length;
        for(int i=0;i<len;i++){
           map.put(inorder[i],i);
       }
        return mybuildTree(preorder,inorder,0,len-1,0,len-1);
    }

    private TreeNode mybuildTree(int[]preorder,int[] inorder,int p_left,int p_right,int in_left,int in_right) {
        if(p_left>p_right){
            return null;
        }
        //前序遍历的第一个结点是根节点
        int p_index=p_left;
        //从而找到根节点在中序遍历的位置
        int in_index=map.get(preorder[p_index]);
        TreeNode head=new TreeNode(preorder[p_index]);
        //根节点左边结点数目
        int left=in_index-in_left;
        //根节点右边结点数目
        //int right=in_right-in_index;
        head.left=mybuildTree(preorder,inorder,p_left+1,p_left+left,in_left,in_index-1);
        head.right=mybuildTree(preorder,inorder,p_left+left+1,p_right,in_index+1,in_right);
        return head;
    }
}
