package cn.LeetCode.第四周.二叉树;
//如此的简单
public class o完全二叉树的节点个数222 {
    int count=0;
    public int countNodes(TreeNode root) {
        dfs(root);
        return count;
    }
    public void dfs(TreeNode root){
        if(root==null){
            return;
        }
        count++;
        dfs(root.left);
        dfs(root.right);
    }
}
