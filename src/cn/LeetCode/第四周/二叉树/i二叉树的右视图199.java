package cn.LeetCode.第四周.二叉树;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class i二叉树的右视图199 {
    //深度优先搜索
    public List<Integer> rightSideView(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        dfs(list, root, 0);
        return list;
    }

    public void dfs(List<Integer> list, TreeNode root, int h) {
        if (root == null) {
            return;
        }
        if (h == list.size()) {
            list.add(root.val);
        }
        dfs(list, root.right, h+1);
        dfs(list, root.left, h+1);


    }
    //广度优先遍历，层次遍历，记录每一层的最后一个元素
    public List<Integer> rightSideView2(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        if(root==null){
            return null;
        }
        Queue<TreeNode> queue=new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()){
            int size=queue.size();
            for(int i=0;i<size;i++){
                TreeNode node=queue.poll();
                if(node.left!=null){
                    queue.offer(node.left);
                }
                if(node.right!=null){
                    queue.offer(node.right);
                }
                //表示当前层最后一个元素，放入集合
                if(i==size-1){
                    list.add(node.val);
                }
            }

        }

        return list;
    }
}



