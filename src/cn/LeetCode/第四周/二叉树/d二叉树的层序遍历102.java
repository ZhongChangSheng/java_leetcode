package cn.LeetCode.第四周.二叉树;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class d二叉树的层序遍历102 {
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> res=new ArrayList<>();
        Queue<TreeNode> queue=new LinkedList<>();
        if(root==null){
            return res;
        }
        queue.add(root);
        while(!queue.isEmpty()){
            int l=queue.size();
            List<Integer> list=new ArrayList<>();
            for(int i=0;i<l;i++){
                TreeNode node=queue.poll();
                list.add(node.val);
                if(node.left!=null)
                    queue.add(node.left);

                if(node.right!=null)
                    queue.add(node.right);

            }
            res.add(list);
        }
        return res;
    }

}
