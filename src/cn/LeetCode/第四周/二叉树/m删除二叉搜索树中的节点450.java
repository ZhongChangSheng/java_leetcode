package cn.LeetCode.第四周.二叉树;

public class m删除二叉搜索树中的节点450 {

    public TreeNode deleteNode(TreeNode root, int key) {
        if(root==null){
            return null;
        }
        if(root.val>key){
            root.left=deleteNode(root.left,key);
        }
        else if(root.val<key){
            root.right=deleteNode(root.right,key);
        }
        else{
            //三种情况
            //1.刚好是叶子结点，直接删除
            if(root.left==null&&root.right==null){
                root=null;
            }
            //2.结点有右节点
            else if(root.right!=null){
                root.val=successor(root);
                root.right=deleteNode(root.right,root.val);
            }
            //3.结点只有左节点
            else{
                root.val=pre(root);
                root.left=deleteNode(root.left,root.val);
            }
        }

        return root;
    }
    public int successor(TreeNode root) {
        root = root.right;
        while (root.left != null) root = root.left;
        return root.val;
    }
    public int pre(TreeNode root) {
        root = root.left;
        while (root.right != null) root = root.right;
        return root.val;
    }
}
