package cn.LeetCode.第四周.二叉树;

import java.util.LinkedList;
import java.util.Queue;

public class t左叶子之和404{
    //当我们遍历到节点node 时，如果它的左子节点是一个叶子结点，那么就将它的左子节点的值累加计入答案。
    public int sumOfLeftLeaves(TreeNode root) {
        if(root==null){
            return 0;
        }
        int sum=0;
        if(root.left!=null&&root.left.left==null&&root.left.right==null){
            sum=root.left.val;
        }
        return sum+sumOfLeftLeaves(root.left)+sumOfLeftLeaves(root.right);
    }
    //深度优先遍历
    public int sumOfLeftLeaves2(TreeNode root) {
        if(root==null){
            return 0;
        }
        Queue<TreeNode> queue=new LinkedList<>();
        queue.offer(root);
        int sum=0;
        while (!queue.isEmpty()){
            TreeNode node=queue.poll();
            if(node.left!=null) {
                if(node.left.left==null&&node.left.right==null){
                    sum+=node.left.val;
            }else{
                queue.offer(node.left);
            }
            }
            if(node.right!=null){
                if(node.right.left!=null||node.right.right!=null){
                    queue.offer(node.right);
                }
            }
        }
       return sum;
    }
}
