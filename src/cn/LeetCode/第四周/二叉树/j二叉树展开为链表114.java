package cn.LeetCode.第四周.二叉树;

import java.util.ArrayList;
import java.util.List;

public class j二叉树展开为链表114 {
    public void flatten(TreeNode root) {
        List<TreeNode> list=new ArrayList<TreeNode>();
        dfs(root,list);
        int len=list.size();
        for(int i=1;i<len;i++){
            TreeNode pre=list.get(i-1);
            TreeNode cur=list.get(i);
            cur.left=null;
            pre.right=cur;
        }
    }
    public void dfs(TreeNode root,List<TreeNode> list){
        if(root==null){
            return;
        }
        list.add(root);
        dfs(root.left,list);
        dfs(root.right,list);
    }
}

