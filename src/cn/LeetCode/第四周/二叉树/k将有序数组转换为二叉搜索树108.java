package cn.LeetCode.第四周.二叉树;

public class k将有序数组转换为二叉搜索树108 {
    public TreeNode sortedArrayToBST(int[] nums) {
        return dfs(nums,0,nums.length-1);
    }
    public TreeNode dfs(int nums[],int left,int right){
        if(left>right){
            return null;
        }
        int mid=(right+1-left)/2+left;
        TreeNode root=new TreeNode(nums[mid]);
        root.left=dfs(nums,left,mid-1);
        root.right=dfs(nums,mid+1,right);
        return root;
    }
}
