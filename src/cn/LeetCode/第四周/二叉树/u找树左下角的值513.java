package cn.LeetCode.第四周.二叉树;

import java.util.Deque;
import java.util.LinkedList;

public class u找树左下角的值513 {
    int res=0;
    int depth=Integer.MIN_VALUE;
    public int findBottomLeftValue2(TreeNode root) {
        dfs(root,0);
        return res;
    }
    public void dfs(TreeNode root,int h){
        if(root==null){
            return;
        }
        if(depth<h){
            depth=h;
            res=root.val;
        }
        dfs(root.left,h+1);
        dfs(root.right,h+1);
    }
    public int findBottomLeftValue(TreeNode root) {
        Deque<TreeNode> deque=new LinkedList<>();
        deque.offer(root);
        TreeNode node=null;
        while(!deque.isEmpty()){
            node=deque.poll();
            if(node.right!=null){
                deque.offer(node.right);
            }
            if(node.left!=null){
                deque.offer(node.left);
            }
        }
        return node.val;
    }
}
