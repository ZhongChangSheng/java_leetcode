package cn.LeetCode.第四周.二叉树;

public class p二叉树的最大深度104 {
    public int maxDepth(TreeNode root) {
        if(root==null){
            return 0;
        }
        return Math.max(maxDepth(root.left),maxDepth(root.right))+1;

    }
}
