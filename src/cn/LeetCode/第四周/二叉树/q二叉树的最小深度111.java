package cn.LeetCode.第四周.二叉树;

public class q二叉树的最小深度111 {
    public int minDepth(TreeNode root) {
        if(root==null){
            return 0;
        }
        int left=minDepth(root.left);
        int right=minDepth(root.right);
        //如果左右结点有一个为空，返回深度更大的那一个，
        // 比如节点的左节点为空，右节点不为空，那它不不是叶子结，应该走右边继续遍历
        if(root.left==null||root.right==null){
            return Math.max(left,right)+1;
        }
        //如果两个都为空，说明是叶子结点，返回最小深度
        return Math.min(left,right)+1;
    }
}
