package cn.LeetCode.第四周.二叉树;

public class y最大二叉树654 {
    public TreeNode constructMaximumBinaryTree(int[] nums) {
        int len=nums.length;
        return build(nums,0,len-1);
    }
    public TreeNode build(int[]nums,int start,int end){
        if(start>end){
            return null;
        }
        int index=0;//记录最大值下标
        int max=Integer.MIN_VALUE;//记录最大值
        for(int i=start;i<=end;i++){
            if(max<nums[i]){
                max=nums[i];
                index=i;
            }
        }
        TreeNode node=new TreeNode(max);//建立根节点
        node.left=build(nums,start,index-1);
        node.right=build(nums,index+1,end);
        return node;


    }

}
