package cn.LeetCode.第四周.二叉树;

import java.util.ArrayList;
import java.util.List;

public class r二叉树的所有路径257 {
    public List<String> binaryTreePaths(TreeNode root) {
        List<String> list=new ArrayList<>();
        dfs(list,root,"");
        return list;
    }
    public void dfs(List<String> list,TreeNode root,String str) {
        if (root == null) {
            return;
        }
        StringBuilder sb = new StringBuilder(str);
        sb.append(Integer.toString(root.val));
        //判断是否叶子结点，是直接将结果添加
        //不是继续遍历
        if (root.left == null && root.right == null) {
            list.add(sb.toString());
        } else {
            sb.append("->");
            dfs(list, root.left, sb.toString());
            dfs(list, root.right, sb.toString());
        }

    }

}
