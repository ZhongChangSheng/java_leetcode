package cn.LeetCode.第四周.二叉树;

public class w二叉搜索树的最近公共祖先235 {
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        //3种情况
        //1.都在结点的左边
        //2.都在结点的右边
        //3.在节点的两侧，直接返回结点
        if(root.val>p.val&&root.val>q.val){
            return lowestCommonAncestor(root.left,p,q);
        }
        else if(root.val<p.val&&root.val<q.val){
            return lowestCommonAncestor(root.right,p,q);
        }
        return root;
    }
}
