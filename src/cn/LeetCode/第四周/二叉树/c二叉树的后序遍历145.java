package cn.LeetCode.第四周.二叉树;

import java.util.ArrayList;
import java.util.List;

public class c二叉树的后序遍历145 {
    List<Integer> res=new ArrayList<>();
    public List<Integer> postorderTraversal(TreeNode root) {
        postorder(root);
        return res;
    }
    public void postorder(TreeNode root){
        if(root==null){
            return;
        }
        postorder(root.left);
        postorder(root.right);
        res.add(root.val);
    }
}
