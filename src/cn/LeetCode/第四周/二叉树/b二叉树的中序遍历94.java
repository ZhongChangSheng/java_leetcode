package cn.LeetCode.第四周.二叉树;

import java.util.ArrayList;
import java.util.List;

public class b二叉树的中序遍历94 {
    List<Integer> res=new ArrayList<>();
    public List<Integer> inorderTraversal(TreeNode root) {
        inorder(root);
        return res;
    }
    public void inorder(TreeNode root){
        if(root==null){
            return;
        }
        inorder(root.left);
        res.add(root.val);
        inorder(root.right);
    }
}
