package cn.LeetCode.第四周.二叉树;

import sun.reflect.generics.tree.Tree;

import java.util.*;

public class e二叉树的锯齿形层序遍历103 {
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> ans = new LinkedList<List<Integer>>();
        if (root == null) {
            return ans;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        boolean isLeft = true;//true代表从左往右，false代表从右往左
         while(!queue.isEmpty()){
             Deque<Integer> deque=new LinkedList<>();
             int n=queue.size();
             for(int i=0;i<n;i++){
                 TreeNode node=queue.poll();
                 if(isLeft){
                     deque.offerLast(node.val);
                 }else{
                     deque.offerFirst(node.val);
                 }
                 if(node.left!=null){
                     queue.offer(node.left);
                 }
                 if(node.right!=null){
                     queue.offer(node.right);
                 }
             }
             List<Integer> list=new LinkedList<>(deque);
             ans.add(list);
             isLeft=!isLeft;
         }
        return ans;
    }

}
