package cn.LeetCode.第四周.二叉树;

public class h二叉树的最近公共祖先236 {
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        //找到p,q结点的位置
        if(root==p||root==q){
            return root;
        }
        if(root!=null){
            //递归从左子树查找
            TreeNode left=lowestCommonAncestor(root.left,p,q);
            //递归从右子树查找
            TreeNode right=lowestCommonAncestor(root.right,p,q);
            //如果left和right都不为空，说明root就是最近祖先
            if(left!=null&&right!=null){
                return root;
            }else if(left==null){
                //都在右子树
                return right;
            }else{
                //都在左子树
                return left;
            }
        }
        return null;
    }

}
