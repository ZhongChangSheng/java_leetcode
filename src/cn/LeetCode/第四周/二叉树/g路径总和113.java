package cn.LeetCode.第四周.二叉树;

import java.util.ArrayList;
import java.util.List;

public class g路径总和113 {
    List<List<Integer>> res=new ArrayList<>();
    List<Integer> list=new ArrayList<>();
    public List<List<Integer>> pathSum(TreeNode root, int targetSum) {
        dfs(root,targetSum,0);
        return res;
    }
    //深度优先遍历，从根节点出发，遍历，如果遍历途中结果不符合，进行回溯
    public void dfs(TreeNode root,int target,int sum){
        if(root==null){
            return;
        }
        //计算结点总和
        sum+=root.val;
        list.add(root.val);
        //如果遍历到哪最后一个结点，sum=target,加入链表
        if(root.left==null&&root.right==null&&sum==target){
            res.add(new ArrayList<>(list));
        }
        //递归遍历左节点
        dfs(root.left,target,sum);
        //递归遍历右节点
        dfs(root.right,target,sum);
        //回溯
        list.remove(list.size()-1);

    }
}
