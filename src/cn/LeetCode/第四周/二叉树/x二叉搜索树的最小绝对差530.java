package cn.LeetCode.第四周.二叉树;

public class x二叉搜索树的最小绝对差530 {
    private Integer min = Integer.MAX_VALUE;

    public int getMinimumDifference(TreeNode root) {
        // 1为可能的最小值
        if (min == 1) {
            return min;
        }
        // 单层逻辑：节点与左子树的最小差为：节点值 - 左子树的最右节点
        if (root.left != null) {
            // 找到左子树的最大节点，即最右节点
            TreeNode leftMax = root.left;
            while (leftMax.right != null) {
                leftMax = leftMax.right;
            }
            min = Math.min(root.val - leftMax.val, min);
            // 左子树
            getMinimumDifference(root.left);
        }
        // 单层逻辑：节点与右子树的最小差为：左子树的最左节点 - 节点值
        if (root.right != null) {
            // 找到右子树的最小节点，即最左节点
            TreeNode rightMin = root.right;
            while (rightMin.left != null) {
                rightMin = rightMin.left;
            }
            min = Math.min(rightMin.val - root.val, min);
            // 右子树
            getMinimumDifference(root.right);
        }
        return min;
    }
}
