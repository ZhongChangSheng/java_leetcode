package cn.LeetCode.第三周.二分查找;

public class 二分查找704 {
    public int search(int[] nums, int target) {
        int len=nums.length;
        int end=len-1;
        int start=0;
        while(start<=end){
            int mid=(start-end)/2+end;
            if(nums[mid]==target){
                return mid;
            }
            else if(nums[mid]<target){
                start=mid+1;
            }
            else{
                end=mid-1;
            }
        }
        return -1;
    }
}
