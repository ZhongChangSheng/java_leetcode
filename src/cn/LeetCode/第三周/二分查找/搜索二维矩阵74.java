package cn.LeetCode.第三周.二分查找;

public class 搜索二维矩阵74 {
    //两次二分查找
    public boolean searchMatrix(int[][] matrix, int target) {
        int len=matrix.length;
        int start=0;
        int end=len-1;
        int l=matrix[0].length;
        while(start<end){
            int mid=(end-start)/2+start;
            if(matrix[mid][0]>=target&&matrix[mid][l-1]<target){
                int i=0,j=matrix[i].length-1;
                while(i<=j){
                    int m=(i+j)/2;
                    if(matrix[mid][m]==target){
                        return true;
                    }
                    else if(matrix[mid][m]<target){
                        i=m+1;
                    }else{
                        j=m-1;
                    }
                }
            }
            if(matrix[mid][0]<target){
                end=mid-1;
            }
            if(matrix[mid][l-1]<target){
                start=mid+1;
            }
        }
       return false;
    }
    //一次二分查找
    public boolean searchMatrix2(int[][] matrix, int target) {
        int m = matrix.length, n = matrix[0].length;
        int low = 0, high = m * n - 1;
        while (low <= high) {
            int mid = (high - low) / 2 + low;
            int x = matrix[mid / n][mid % n];
            if (x < target) {
                low = mid + 1;
            } else if (x > target) {
                high = mid - 1;
            } else {
                return true;
            }
        }
        return false;
    }

}
