package cn.LeetCode.第三周.二分查找;

public class 搜索插入位置35 {
    public int searchInsert(int[] nums, int target) {
            int len=nums.length;
            int start=0;
            int end=len-1;
            while(start<=end){
                int mid=(start-end)/2+end;
                if(nums[mid]<target){
                    start=mid+1;
                }
                else{
                    end=mid-1;
                }
            }
            return start;
        }
}
