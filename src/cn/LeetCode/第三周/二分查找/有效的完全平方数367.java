package cn.LeetCode.第三周.二分查找;

public class 有效的完全平方数367 {
    public boolean isPerfectSquare(int num) {
        int start=0;
        int end=num;
        while(start<=end){
            int mid=(end-start)/2+start;
            long res=(long)mid*mid;
            if(res==num){
                return true;
            }
            else if(res<num){
                start=mid+1;
            }
            else{
                end=mid-1;
            }
        }
        return false;
    }
}
