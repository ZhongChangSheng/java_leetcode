package cn.LeetCode.第三周.二分查找;

public class 第一个错误的版本278 {
    public int firstBadVersion(int n) {
        int start=1;
        int end=n;
        while(start<end){
            int mid=start+(end-start)/2;

            if(!isBadVersion(mid)){
                start=mid+1;
            }
            else{
                end=mid;
            }
        }
        return start;
    }

    private boolean isBadVersion(int mid) {
        return true;
    }
}
