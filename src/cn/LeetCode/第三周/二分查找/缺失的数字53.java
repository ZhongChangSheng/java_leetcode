package cn.LeetCode.第三周.二分查找;

public class 缺失的数字53 {
    public int missingNumber(int[] nums) {
        int n=nums.length;
        int start=0;
        int end=n-1;
        while(start<=end){
            int mid=(end-start)/2+start;
            if(nums[mid]==mid){
                start=mid+1;
            }
            else{
                end=mid-1;
            }
        }
        return start;
    }
}
