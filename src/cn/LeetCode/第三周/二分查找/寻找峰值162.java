package cn.LeetCode.第三周.二分查找;

public class 寻找峰值162 {
    //遍历
    public int findPeakElement(int[] nums) {
        for (int i = 0; i < nums.length; ++i) {
            if (nums[i] > nums[i+1]) {
                return i;
            }
        }
        return nums.length-1;
    }
    //二分查找
    public int findPeakElement2(int[] nums) {
        int n = nums.length;
        if (n == 1){
            return 0;
        }
        if (nums[0] > nums[1]){
            return 0;
        }
        if (nums[n-1] > nums[n - 2]){
            return n-1;
        }
        int left = 1, right = n - 2, ans = -1;
        while (left <= right) {
            int mid = (left + right) / 2;
            if (nums[mid] > nums[mid - 1] && nums[mid] > nums[mid + 1]) {
                ans = mid;
                break;
            }
            if (nums[mid] < nums[mid + 1]) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return ans;
    }
}
