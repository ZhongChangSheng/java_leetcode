package cn.LeetCode.第三周.二分查找;

import java.util.Arrays;

public class 有效三角形的个数611 {
    //三角形成立条件，两边之和大于第三边
    //先排序，a<=b<=c,如果a+b>c,则一定成立
    //使用二重遍历，保证i<j，不重复
    //再利用二分查找在j+1~len-1找到满足三角形的最大边，将其赋值给k,那么[j+1,k]这一段的数都符合三角形
    public int triangleNumber(int[] nums) {
        int len = nums.length;
        Arrays.sort(nums);
        int res = 0;
        for (int i = 0; i < len; ++i) {
            for (int j = i + 1; j < len; ++j) {
                int left = j + 1, right = len - 1, k = j;
                while (left <= right) {
                    int mid = (right-left) / 2+left;
                    //a<=b<=c,如果a+b>c,则一定成立
                    if (nums[mid] < nums[i] + nums[j]) {
                        k = mid;
                        left = mid + 1;
                    } else {
                        right = mid - 1;
                    }
                }
               res += k - j;
            }
        }
        return res;
    }
}
