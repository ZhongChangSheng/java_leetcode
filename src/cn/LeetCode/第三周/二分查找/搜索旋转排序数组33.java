package cn.LeetCode.第三周.二分查找;

/**
 * 输入：nums = [4,5,6,7,0,1,2], target = 0
 * 输出：4
 */
public class 搜索旋转排序数组33 {
    public int search(int[] nums, int target) {
        int len=nums.length;
        int start=0;
        int end=len-1;
        if(len==0){
            return -1;
        }
        if(len==1){
            return nums[0]==target?0:-1;
        }
        while(start<=end){
            int mid=(end-start)/2+start;
            if(nums[mid]==target){
                return mid;
            }

            if(nums[mid]>=nums[0]){
                //如果中间的数大于等于第一个数，并且目标值在两者中间，则移动后面的指针
              if(nums[0]<=target && target<nums[mid]){
                  end=mid-1;
              }else{
                  //如果第一个数大于目标值并且目标值大于中间的数，移动开始指针
                  start=mid+1;
              }
            }
            else{
                //如果中间的数小于等于第一个数，目标值在中间和最后之间，则移动前面的指针
                if (nums[mid] < target && target <= nums[len - 1]) {
                    start = mid + 1;
                } else {
                    //，目标值在中间和开始之间，则移动前面的指针
                    end = mid - 1;
                }
            }
        }
        return -1;
    }
}
