package cn.LeetCode.第三周.二分查找;

public class 山脉数组的封顶索引852 {
    public int peakIndexInMountainArray(int[] arr) {
        int start=0;
        int end=arr.length-1;
        int res=0;
        while(start<end){
            int mid=(end-start)/2+start;
            if(arr[mid]>=arr[mid-1]&&arr[mid]>=arr[mid+1]){
                res=mid;
            }
            if(arr[mid]>=arr[mid-1]){
                start=mid;
            }
            if(arr[mid]>=arr[mid+1]){
                end=mid;
            }
        }
        return res;
    }
}
