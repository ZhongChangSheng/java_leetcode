package cn.LeetCode.第三周.回溯算法;

import java.util.ArrayList;
import java.util.List;

/**
 * 给定两个整数 n 和 k，返回范围 [1, n] 中所有可能的 k 个数的组合。
 *
 * 你可以按 任何顺序 返回答案。
 */
public class h组合77 {
    List<List<Integer>> list=new ArrayList<>();
    List<Integer> list2=new ArrayList<>();
    public List<List<Integer>> combine(int n, int k) {
          dfs(n,1,k);
          return list;
    }
    public void dfs(int n,int cur,int k){
         if(list2.size()==k){
             list.add(new ArrayList<>(list2));
             return;
         }
         for(int i=cur;i<=n-(k-list2.size())+1;i++){
             list2.add(i);
             dfs(n,i+1,k);
             //回溯
             list2.remove(list2.size()-1);
         }
    }
}
