package cn.LeetCode.第三周.回溯算法;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class j分隔回文串131 {
    //思路，遍历字符串，搜索，判断是否回文子串
    List<List<String>> res=new ArrayList<>();
    List<String> list=new ArrayList<>();
    boolean [][]flag;//flag[i][j]=true表示i~j范围是回文子串
    public List<List<String>> partition(String s) {
        int len=s.length();
        flag=new boolean[len][len];
        for(int i=0;i<len;i++){
            //每一个字母都是回文子串
            Arrays.fill(flag[i],true);
        }
        for(int i=len-1;i>=0;i--){
            for(int j=i+1;j<len;j++){
                flag[i][j]= s.charAt(i)==s.charAt(j)&&flag[i+1][j-1];
            }
        }
        dfs(s,0);
        return res;
    }
    public void dfs(String s,int left){
           if(left==s.length()){
               res.add(new ArrayList<>(list));
               return;
           }
           for(int i=left;i<s.length();i++){
               if(flag[left][i]){
                   list.add(s.substring(left,i+1));
                   dfs(s,i+1);
                   list.remove(list.size()-1);

               }
           }
    }

}
