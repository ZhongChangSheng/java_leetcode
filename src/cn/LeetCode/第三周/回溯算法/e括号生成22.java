package cn.LeetCode.第三周.回溯算法;

import java.util.ArrayList;
import java.util.List;

public class e括号生成22 {
    public List<String> generateParenthesis(int n) {
        List<String> list=new ArrayList<>();
        dfs(list,n,0,"");
        return list;
    }
    public void dfs(List<String> list,int left,int right,String str){
        //如果左括号数和有括号数剩余为0
        //则添加一种结果
           if(left==0&&right==0){
               list.add(str);
               return;
           }
           //如果剩余左括号数等于有括号数，只能先左括号
          if(left==right){
             dfs(list,left-1,right,str+'(');
          }//如果剩余左括号数小于有括号数，那么两个都能用
          else{
             if(left>0){
                 dfs(list,left-1,right,str+'(');
             }
             dfs(list,left,right-1,str+')');

    }
    }
}
