package cn.LeetCode.第三周.回溯算法;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class d组合总和II40 {
    List<List<Integer>> list = new ArrayList<>();
    List<Integer> list2 = new ArrayList<>();

    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        Arrays.sort(candidates);
        dfs(candidates,target,0,0);
        return list;
    }

    public void dfs(int[] candidates, int target, int sum, int begin) {
        if (sum == target) {
            list.add(new ArrayList<>(list2));
            return;
        }
        for (int i = begin; i < candidates.length; i++) {
            //去除重复的数据
            if (i > begin && candidates[i] == candidates[i - 1]) continue;
            int rs = candidates[i] + sum;
            if (rs <= target) {
                list2.add(candidates[i]);
                dfs(candidates, target, rs, i + 1);
                //回溯
                list2.remove(list2.size() - 1);
            } else {
                break;
            }
        }
    }
}
