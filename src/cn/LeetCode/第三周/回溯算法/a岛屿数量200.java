package cn.LeetCode.第三周.回溯算法;
/**
 * 输入：grid = [
 *   ["1","1","1","1","0"],
 *   ["1","1","0","1","0"],
 *   ["1","1","0","0","0"],
 *   ["0","0","0","0","0"]
 * ]
 * 输出：1
 */
public class a岛屿数量200 {
    //深度优先搜索
    public int numIslands(char[][] grid) {
        if(grid==null||grid.length==0){
            return 0;
        }

        int row=grid.length;
        int cow=grid[0].length;
        //标记数组
        int [][]visit=new int[row][cow];
        int count=0;
        for(int i=0;i<row;i++){
            for(int j=0;j<cow;j++){
                //遇到1开始搜索
                if(grid[i][j]=='1'&&visit[i][j]!=1){
                    count++;
                    dfs(grid,i,j,visit);
                }
            }
        }
        return count;
    }
    void dfs(char[][]grid,int r,int c,int[][]visit){
        int row=grid.length;
        int cow=grid[0].length;
        //递归结束条件
        if(r<0||c<0||r>=row||c>=cow||grid[r][c]=='0'||visit[r][c]==1){
            return;
        }
        //每次遇到'1'标记;
        visit[r][c]=1;
        //搜索陆地的下边
        dfs(grid,r+1,c,visit);
        //搜索陆地的右边
        dfs(grid,r,c+1,visit);
        //搜索陆地的上边
        dfs(grid,r-1,c,visit);
        //搜索陆地的左边
        dfs(grid,r,c-1,visit);
    }
}
