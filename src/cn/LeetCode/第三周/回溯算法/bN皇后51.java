package cn.LeetCode.第三周.回溯算法;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class bN皇后51 {
    List<List<String>> list=new ArrayList<>();
    public List<List<String>> solveNQueens(int n) {

        char [][]chess=new char[n][n];
        for(char []ch:chess){
            Arrays.fill(chess,'.');
        }
        dfs(0,chess);
        return list;
    }
    public void dfs(int row,char[][]chess){
        if(row==chess.length){
            list.add(charToList(chess));
            return;
        }
        int n=chess[row].length;
        for(int i=0;i<n;i++){
            if(check(chess,row,i)){
                chess[row][i]='Q';
                dfs(row+1,chess);
                //回溯
                chess[row][i]='.';
            }
        }
    }
    public boolean check(char[][]chess,int x,int y){
        int len=chess.length;
        //因为是从行开始遍历的，所以不需要判断是否在同一行，下半部分也不要判断
        for(int i=0;i<x;i++){
            //判断是否在同一列(x的后面还没开始放，不需要判断)
            if(chess[i][y]=='Q'){
                return false;
            }
        }
        // 检查右上方是否有皇后冲突
        for (int i = x - 1, j = y + 1; i >=0 && j <len; i--, j++) {
            if (chess[i][j] == 'Q') {
                return false;
            }
        }
        // 检查左上方是否有皇后冲突
        for (int i = x-1, j = y -1; i>=0&& j >=0; i--, j--) {
            if (chess[i][j] == 'Q') {
                return false;
            }
        }
        return true;
    }
    public List charToList(char[][] board) {
        List<String> list = new ArrayList<>();
        for (char[] c : board) {
            list.add(String.copyValueOf(c));
        }
        return list;
    }

}
