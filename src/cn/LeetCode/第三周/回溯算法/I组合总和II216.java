package cn.LeetCode.第三周.回溯算法;

import java.util.ArrayList;
import java.util.List;

/**
 * 找出所有相加之和为 n 的 k 个数的组合，且满足下列条件：
 */
public class I组合总和II216 {
    List<List<Integer>> res=new ArrayList<>();
    List<Integer> list=new ArrayList<>();
    public List<List<Integer>> combinationSum3(int k, int n) {
        dfs(k,n,1,0);
        return res;
    }
    public void dfs(int k,int n,int cur,int sum){
        if(list.size()==k&&sum==n){
            res.add(new ArrayList<>(list));
            return;
        }
        for(int i=cur;i<=9;i++){
            //剪枝
            if(sum>n){
                return;
            }
                list.add(i);
                dfs(k,n,i+1,sum+i);
                //回溯
                list.remove(list.size()-1);
        }
    }

}
