package cn.LeetCode.第三周.回溯算法;

import java.util.Arrays;

public class f火柴拼正方形473 {
    boolean[]use;
    public boolean makesquare2(int[] matchsticks) {
        int c=0;
        Arrays.sort(matchsticks);
        for(int i=0;i<matchsticks.length;i++){
            c+=matchsticks[i];
        }
        if(c%4!=0){
            return false;
        }

        int len=c/4;
        if(matchsticks[matchsticks.length-1]>len){
            return false;
        }
        use=new boolean[matchsticks.length];
        return  dfs(matchsticks,0,0,4,len);
    }
    public boolean dfs(int[]m,int index,int curlen,int k,int len){
          if(k==1){
                  return true;
          }
          if(curlen==len){
              return dfs(m,0,0,k-1,len);
          }
          for(int i=index;i<m.length&&curlen+m[i]<=len;i++){
              if(use[i]){
                  continue;
              }
              use[i]=true;
              if(dfs(m,i+1,curlen+m[i],k,len)){
                  return true;
              }
              use[i]=false;
              while(i<m.length-1&&m[i+1]==m[i]){
                  i++;
              }
          }
          return false;
    }

    //你要用 所有的火柴棍 拼成一个正方形。不能折断 任何一根火柴棒，但你可以把它们连在一起，而且每根火柴棒必须使用一次
    public boolean makesquare(int[] nums) {
        int sum = 0;
        boolean []used = new boolean[nums.length];
        Arrays.sort(nums);
        for(int n : nums) {
            sum += n;
        }
        //如果火柴长度和不是四的倍数，那就没必要拼了
        if(sum % 4 != 0) {
            return false;
        }
        //求出正方形边的长度
        int len =  sum / 4;
        //如果最长的火柴大于边的长度，也拼不成
        if(nums[nums.length - 1] > len){
            return false;
        }
        return dfs(nums, 0, 0, 4, len, used);
    }
public boolean dfs(int[] nums, int startIndex, int curSum, int k, int len, boolean []used){
    if(k == 1){
        return true;
    }
    if(curSum ==len) {
        return dfs(nums, 0, 0, k - 1, len, used);//找到了一个组合,还有k-1个
    }
    for (int i = startIndex; i < nums.length && curSum + nums[i] <= len; i++) {//剪枝
        if(used[i]) {
            continue;//使用过的元素就不能再使用了,跳过
        }
        used[i] = true;//添加元素nums[i]
        if(dfs(nums,i+1,curSum+nums[i], k,len,used)){
            return true;//如果添加这个元素后，完成了题目要求，就return true.
        }
        used[i] = false;//回溯
        while(i<nums.length - 1&&nums[i+1]==nums[i]){
            //剪枝3 ,非常重要，大幅提升效率
             i++; //如果某个元素无法使得组合达到目标值，那么跟它值相同的元素也不需要添加了
        }

    }
    return false;
}


}
