package cn.LeetCode.第三周.回溯算法;

import java.util.ArrayList;
import java.util.List;

public class k全排列46 {
    List<List<Integer>> res=new ArrayList<>();
    public List<List<Integer>> permute(int[] nums) {
        dfs(nums,0);
        return res;
    }
    public void dfs(int[]nums,int k){
        List<Integer> list=new ArrayList<>();
        if(k==nums.length-1){
            for(int i=0;i<nums.length;i++){
                list.add(nums[i]);
            }
            res.add(list);
            return;
        }
        for(int i=k;i<nums.length;i++){
            //通过不断交换数字来进行排列
            int tem=nums[k];
            nums[k]=nums[i];
            nums[i]=tem;
            dfs(nums,k+1);
            //回溯
            int tem2=nums[k];
            nums[k]=nums[i];
            nums[i]=tem2;
        }

    }
}
