package cn.LeetCode.第三周.回溯算法;

import java.util.ArrayList;
import java.util.List;

public class c子集78 {
    List<List<Integer>> list=new ArrayList<>();
    List<Integer> list1=new ArrayList<>();
    public List<List<Integer>> subsets(int[] nums) {

         dfs(nums,0);
         return list;
    }
    public void dfs(int[] nums,int k){
             if(k==nums.length){
                 list.add(new ArrayList<>(list1));
                 return;
             }

             list1.add(nums[k]);
             dfs(nums,k+1);
             list1.remove(list1.size()-1);
             dfs(nums,k+1);
    }
}
