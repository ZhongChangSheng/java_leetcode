package cn.LeetCode.第三周.搜索算法;

public class 数组中的逆序对51 {
    //这是第一时间想出的办法，显然超时了，再想想其他办法吧
    public int reversePairs(int[] nums) {
        int n=nums.length;
        int count=0;
        for(int i=0;i<n-1;i++){
            for(int j=i+1;j<n;j++){
                if(nums[i]>nums[j]){
                    count++;
                }
            }
        }
        return count;
    }
    //利用归并排，在排序过程中计算逆序对个数
    public int reversePairs2(int[] nums) {
        int len = nums.length;
        if (len < 2) {
            return 0;
        }
        int[] copy = new int[len];
        for (int i = 0; i < len; i++) {
            copy[i] = nums[i];
        }
        int[] temp = new int[len];
        return reversePairs(nums, 0, len - 1, temp);
    }

    private int reversePairs(int[] nums, int left, int right, int[] temp) {
        if (left == right) {
            return 0;
        }
        int mid = left + (right - left) / 2;
        int leftPairs = reversePairs(nums, left, mid, temp);//归并排序左边的部分
        int rightPairs = reversePairs(nums, mid + 1, right, temp);//归并排序右边的部分
        if (nums[mid] <= nums[mid + 1]) {
            return leftPairs + rightPairs;
        }
        int crossPairs = mergeAndCount(nums, left, mid, right, temp);
        return leftPairs + rightPairs + crossPairs;
    }

    private int mergeAndCount(int[] nums, int left, int mid, int right, int[] temp) {
        for (int i = left; i <= right; i++) {
            temp[i] = nums[i];
        }
        int i = left;
        int j = mid + 1;
        int count = 0;
        for (int k = left; k <= right; k++) {
            if (i == mid + 1) {
                nums[k] = temp[j];
                j++;
            } else if (j == right + 1) {
                nums[k] = temp[i];
                i++;
            } else if (temp[i] <= temp[j]) {
                nums[k] = temp[i];
                i++;
            } else {
                nums[k] = temp[j];
                j++;
                count += (mid - i + 1);
            }
        }
        return count;
    }
    //归并排序
    private int mergeAndCount2(int[] nums, int left, int mid, int right, int[] temp) {
        for(int i=left;i<=right;i++){
            temp[i]=nums[i];
        }
          int count=0;//记录逆序对的个数
          int start=left;
          int end=mid+1;
          for(int i=left;i<=right;i++){
              if(start==mid+1){
                  //左边到达边界
                  nums[i]=temp[end];
                  end++;
              }
              else if(end==right+1){
                  //右边到达边界
                  nums[i]=temp[start];
                  start++;
              }
              else if(temp[start]<=temp[end]){
                  nums[i]=temp[start];
                  start++;
              }else{
                  //只有当右边的数小于左边的数才需要计算逆序对的个数
                  //且个数是mid-start+1;
                  nums[i]=temp[end];
                  end++;
                  count+=mid-start+1;
              }
          }
          return count;
    }
}
