package cn.LeetCode.第三周.位运算;

public class h汉明距离461 {
    public int hammingDistance(int x, int y) {
        int xor=0;
        xor=x^y;//异或结果中1的个数就是x,y之间的汉明距离
        int count=0;
        while(xor!=0){
            count+=xor&1;//用xor的最后一位与1进行与运算，最后一位为1，xor^1=1;
            xor>>=1;//右移
        }
        return count;
    }
}
