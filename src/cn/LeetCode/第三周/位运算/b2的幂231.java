package cn.LeetCode.第三周.位运算;

public class b2的幂231 {
    //利用与运算，4(100) & 3(011) ==0,与运算只有两个都为1才是1
    public boolean isPowerOfTwo(int n) {
        return n > 0 && (n & (n - 1)) == 0;
    }
}
