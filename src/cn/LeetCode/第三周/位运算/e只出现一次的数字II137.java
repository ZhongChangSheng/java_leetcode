package cn.LeetCode.第三周.位运算;

import java.util.HashMap;
import java.util.Map;

public class e只出现一次的数字II137 {
    //使用hashMap统计每一个出现的次数,在遍历map找到次数为一的key;
    public int singleNumber(int[] nums) {
        int res=0;
     Map<Integer,Integer> map=new HashMap<>();
        for (int num : nums) {
            map.put(num,map.getOrDefault(num,0)+1);
        }
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
           if(entry.getValue()==1) {
               res=entry.getKey();
           }
        }
        return  res;
    }
    //先统计所有数各个二进制位出现的次数
    //在对3求余，最后组成的数就是只出现一次的二进制数，再将它转为十进制
    public int singleNumber2(int[] nums) {
        int []c=new int[32];
        for (int num : nums) {
            for(int i=0;i<32;i++){
                if(((num>>i)&1)==1){
                    c[i]++;
                }
            }
        }
        int res=0;
        for(int i=0;i<32;i++){
            if((c[i]%3&1)==1){
                res+=(1<<i);
            }
        }
        return  res;
    }
}
