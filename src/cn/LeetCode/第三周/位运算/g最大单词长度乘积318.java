package cn.LeetCode.第三周.位运算;

public class g最大单词长度乘积318{
    public int maxProduct(String[] words) {
        int len= words.length;
        int []res=new int[len];
        for(int i=0;i<len;i++){
            String word=words[i];
            for(int j=0;j<word.length();j++){
               int m=word.charAt(j)-'a';
               //将每一个字母转换成的二进制的某一位，如a放在第一位，b放在第二位。。。。，同时左移放好位置
                //"abcf" z最终变为 100111->39 res[0]=39;
               res[i]|=1<<m;
            }
    }
        int mut=0;
        for(int i=0;i<len;i++){
            for(int j=i+1;j<len;j++){
                //判断是否有共同的字母，使用与运算没有相同的则为0
                if((res[i]&res[j])==0){
                   mut=Math.max(mut,words[i].length()*words[j].length());
                }
            }
        }
        return mut;
    }
}
