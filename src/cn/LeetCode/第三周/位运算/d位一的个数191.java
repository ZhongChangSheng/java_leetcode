package cn.LeetCode.第三周.位运算;

public class d位一的个数191 {
    public static int hammingWeight(int n) {
        int count=0;
        for(int i=0;i<32;i++){
            //从n的最右边一位和1进行&运算，等于1，则一的个数加一
            if(((n>>i)&1)==1){
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        int n=3;
        System.out.println(hammingWeight(n));
    }
}
