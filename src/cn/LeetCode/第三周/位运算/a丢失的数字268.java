package cn.LeetCode.第三周.位运算;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class a丢失的数字268 {
    //这是我第一时间想出来的办法
    //时间复杂度O(nlogn)有点高，耗内存
    public int missingNumber(int[] nums) {
        Arrays.sort(nums);
        for(int i=0;i<nums.length;i++){
            if(nums[i]!=i){
                return i;
            }
        }
        return nums.length;
    }
    //使用set集合,时间复杂度O(n)还是有点高
    public int missingNumber2(int[] nums) {
        Set<Integer> set=new HashSet<>();
        for(int i=0;i<=nums.length;i++){
            set.add(i);
        }
        int res=-1;
        for(int i=0;i<nums.length;i++){
            if(!set.contains(nums[i])){
                res=i;
                break;
            }
        }
        return res;
    }
    //使用位运算
    public static int missingNumber3(int[] nums) {
        int xor=0;
        for(int i=0;i<nums.length;i++){
          xor^=nums[i];
        }
        for(int i=0;i<=nums.length;i++){
            xor^=i;
        }
        return xor;
    }

    public static void main(String[] args) {
        int []nums={0,1,3};
        System.out.println(missingNumber3(nums));
    }

}
