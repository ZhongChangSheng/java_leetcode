package cn.LeetCode.第三周.位运算;

import java.util.HashMap;
import java.util.Map;

public class f只出现一次的数字III260 {
    public int[] singleNumber(int[] nums) {
        int[] res=new int[2];

        Map<Integer,Integer> map=new HashMap<>();
        for (int num : nums) {
            map.put(num,map.getOrDefault(num,0)+1);
        }
        int index=0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if(entry.getValue()==1){
               res[index++]=entry.getKey();
            }
        }
        return res;
    }
    public int[] singleNumber2(int[] nums) {
        int[] res=new int[2];
        int xor=0;
        for (int num : nums) {
            xor^=num;
        }
       int m=xor&(-xor);
        for (int num : nums) {
            if((m&num)==0){
                res[0]^=num;
            }else{
                res[1]^=num;
            }
        }
        return res;
    }
}
