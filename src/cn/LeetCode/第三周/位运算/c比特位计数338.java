package cn.LeetCode.第三周.位运算;

public class c比特位计数338 {
    //x&(x-1)表示将x的最后一位1变为0
    public int[] countBits(int n) {
        int[] res=new int[n+1];
        for(int i=0;i<=n;i++){
            int count=0;
            int x=i;
            while(x>0){
                x&=x-1;
                count++;
            }
            res[i]=count;
        }
        return res;
    }
}
