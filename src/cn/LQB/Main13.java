package cn.LQB;

/**
 * 对于长度为5位的一个01串，每一位都可能是0或1，一共有32种可能。它们的前几个是：
 * 00000
 * 00001
 * 00010
 * 00011
 * 00100
 * 请按从小到大的顺序输出这32种01串。
 */
public class Main13 {
    public static void main(String[] args) {
       int a[]={0,0,0,0,0};
       for(int i=0;i<5;i++){
           System.out.print(a[i]);
       }
       while (a[0]+a[1]+a[2]+a[3]+a[4]<5){
           a[4]=a[4]+1;
           System.out.println();
           for(int i=4;i>0;i--){
               if(a[i]==2){
                   a[i-1]=a[i-1]+1;
                   a[i]=0;
               }
           }
           for(int i=0;i<5;i++){
               System.out.print(a[i]);

           }
       }

    }
}
