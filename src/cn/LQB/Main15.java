package cn.LQB;

import java.util.Scanner;

/**
 * Fibonacci数列的递推公式为：Fn=Fn-1+Fn-2，其中F1=F2=1。
 *
 * 当n比较大时，Fn也非常大，现在我们想知道，Fn除以10007的余数是多少。
 * 用数组，递归会超时
 */
public class Main15 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int a[]=new int[n];
        if(n<3){
            System.out.println(1);
        }
        else {
            a[0]=1;
            a[1]=1;
            for(int i=2;i<n;i++){
                a[i]=(a[i-1]+a[i-2])%10007;
            }
            System.out.println(a[n-1]);
        }
    }
}
