package cn.LQB;

import java.util.Scanner;

public class HuiWen {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        for(int i=11;i<1000000;i++){
            if (i < 100000) {
                int a = i / 10000;// 万位
                int b = i / 1000 % 10;// 千位
                int c = i / 100 % 10;// 百位
                int d = i / 10 % 10;// 十位
                int e = i % 10;// 个位
                if (a == e && b == d && a + b + c + d + e == n) {
                    System.out.println(i);
                }
            } else {
                int f = i / 100000;// 十万位
                int g = i / 10000 % 10;// 万位
                int h = i / 1000 % 10;// 千位
                int j = i / 100 % 10;// 百位
                int k = i / 10 % 10;// 十位
                int l = i % 10;// 个位
                if (f == l && g == k && h == j && f + g + h + j + k + l == n) {
                    System.out.println(i);
                }
            }}
    }
}
