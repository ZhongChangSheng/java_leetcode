package cn.LQB;

import java.util.Arrays;
import java.util.Scanner;

public class 后缀表达式 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int m=sc.nextInt();
        int k=n+m+1;
        int[]a=new int[k];
        int sum=0;
        int count=0;//负数的个数
        for(int i=0;i<k;i++){
            a[i]=sc.nextInt();
            sum+=a[i];
            if(a[i]<0)
                count++;
        }

        Arrays.sort(a);
        if(m==0){
            System.out.println(sum);
        }else {
            if(count>0){
                if(count==k){
                    for(int i=0;i<k-1;i++)
                    {
                        sum-=2*a[i];
                    }
                }else {
                    for(int i=0;i<k;i++)
                    {
                        sum-=2*a[i];
                    }
                }
            }
            else
                sum-=2*a[0];
            System.out.println(sum);
        }

    }
}
