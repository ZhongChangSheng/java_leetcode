package cn.LQB;

import java.util.Arrays;
import java.util.Scanner;

public class MaxAMin {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int a[]=new int[n];
        int b=0;
        for(int i=0;i<n;i++){
            a[i]=sc.nextInt();
            b=b+a[i];
        }
        Arrays.sort(a,0,n);
        System.out.println(a[n-1]);
        System.out.println(a[0]);
        System.out.println(b);
    }
}
