package cn.LQB;

import java.util.Scanner;

public class Beautiful {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int m=sc.nextInt();
        char s[][]=new char[n][m];
        for(int i=0;i<m;i++){
            s[0][i]=(char)('A'+i);
        }
        for(int j=0;j<n;j++){
            s[j][0]=(char)('A'+j);
        }
        for(int i=1;i<n;i++){
            for(int j=1;j<m;j++){
                s[i][j]=s[i-1][j-1];
            }
        }
        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                System.out.print(s[i][j]);
            }
            System.out.println();
        }
    }
}
