package cn.LQB;

import java.util.Scanner;

public class MainN3 {
    private static int sum;
    private static int n;
    static int a1[];
    static boolean bool=true;
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Scanner sc=new Scanner(System.in);
        n = sc.nextInt();
        sum = sc.nextInt();
        int a[]=new int[n];
        int vis[]=new int[n+1];

        dfs(0,a,vis,bool);
    }
    //状态的改变是，1.长度，如果长度位4就正确，一棵树一样的进行搜索，还要判段是否加起来等于数
    public static void dfs(int setp,int[] a,int[]vis,boolean b){
        if(setp==n){
            int[] a1=new int[n];
            //复制,因为最后要加总和，一直加到1位数，所以要再有一个数组
            for(int i=0;i<n;i++)
                a1[i]=a[i];
            //外层是次数,下一层的长度，即这一层的长度减1
            for(int i=1;i<n;i++){
                for(int j=0;j<n-1;j++){
                    a1[j]=a1[j]+a1[j+1];
                }
            }
            if(a1[0]==sum){
                for(int x:a){
                    System.out.print(x+" ");
                }
                //当相等的时候就可以不再找了
                //有多值，这个就说下面还有多的值就不输入进来了
                bool=false;
                return;
            }else
                return;
        }
        if(bool==true){
            for(int i=1;i<=n;i++){//1到数字n
                if(vis[i]==0){//没被访问过
                    a[setp]=i;//赋值
                    vis[i]=1;
                    dfs(setp+1,a,vis,bool);
                    vis[i]=0;
                }
            }
        }
        return;
    }

}
