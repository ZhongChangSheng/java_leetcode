package cn.LQB.贪心;

import java.util.Scanner;

/**
 * 　有n个人正在饭堂排队买海北鸡饭。每份海北鸡饭要25元。
 * 奇怪的是，每个人手里只有一张钞票（每张钞票的面值为25、50、100元），
 * 而且饭堂阿姨一开始没有任何零钱。请问饭堂阿姨能否给所有人找零（假设饭堂阿姨足够聪明）
 * 第一行一个整数n，表示排队的人数。
 4
 25 25 50 50
 YES
 * 　　接下来n个整数a[1],a[2],...,a[n]。a[i]表示第i位学生手里钞票的价值（i越小，在队伍里越靠前）
 */
//如果是50,那么25的零钱数量要>0;如果是100,那么25的零钱数量要>=3或者 25>0并且50>0;
public class 找零钱 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int []a=new int[n];
        int count1=0;//阿姨手上零钱的数量
        int count2=0;
        int count3=0;
        boolean flag=true;
        for(int i=0;i<n;i++){
            a[i]=sc.nextInt();
        }
        for(int i=0;i<n;i++){
            if(a[i]==50){
                if(count1>0){
                    count1--;
                    count2++;
                }
                else {
                    flag=false;
                    break;
                }
            }
            else if(a[i]==100){
                if(count2>0&&count1>0){
                    count1--;
                    count2--;
                    count3++;
                }
                else if(count1>=3){
                    count1-=3;
                }
                else {
                    flag=false;
                    break;
                }
            }
            else {
                count1++;
            }
        }
        if(flag){
            System.out.println("YES");
        }else
            System.out.println("NO");



    }
}
