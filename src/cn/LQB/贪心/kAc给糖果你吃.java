package cn.LQB.贪心;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 　kAc有n堆糖果，每堆有A[i]个。
 * 　　kAc说你只能拿m次糖果，聪明的你当然想要拿最多的糖果来吃啦啦啦~
 * 　　//第二天，kAc问你还想吃糖果么？（嘿嘿嘿）说着眼角路出奇怪的微笑...
 *
 */
public class kAc给糖果你吃 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int m=sc.nextInt();
        int[] a=new int[n];
        for(int i=0;i<n;i++){
            a[i]=sc.nextInt();
        }
        long res=0;
        Arrays.sort(a);
        for(int i=0;i<m;i++){
            res+=a[a.length-i-1];//从最大的开始拿
        }
        System.out.println(res);
    }
}
