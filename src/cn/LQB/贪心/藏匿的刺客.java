package cn.LQB.贪心;

import java.util.Scanner;

/**
 * 强大的kAc建立了强大的帝国，但人民深受其学霸及23文化的压迫，于是勇敢的鹏决心反抗。
 * 　　kAc帝国防守森严，鹏带领着小伙伴们躲在城外的草堆叶子中，称为叶子鹏。
 * 　　kAc帝国的派出的n个看守员都发现了这一问题，第i个人会告诉你在第li个草堆到第ri个草堆里面有人，
 *    要求你计算所有草堆中最少的人数，以商议应对。
 * 　 “你为什么这么厉害”，得到过kAc衷心赞美的你必将全力以赴。
 * 第一行一个数字n，接下来2到n+1行，每行两个数li和ri，如题
 * 将每个区间按右端点排序
 *  *
 *  * 将最小的右端点和下一个区间的左端点比较，若左端点小于右端点，说明两区间重合，只要在该右端点记一个人即可
 *  *
 *  * 若左端点大于右端点，说明两区间未重合，需要再记一个人，并且更新最小右端点为该区间的右端点
 *  *
 *  * 遍历所有区间
 *  * 1 3
 *  * 2 4
 *  * 5 7
 *  * 1 8
 *  * 8 8
 */
public class 藏匿的刺客 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in) ;
        int n=sc.nextInt();
        int count=1;
        int []a=new int[n];
        int []b=new int[n];
        for(int i=0;i<n;i++){
            a[i]=sc.nextInt();
            b[i]=sc.nextInt();
        }
        int t;
        for(int i=0;i<n;i++){
            for(int j=0;j<n-i-1;j++){
                if(b[j]>b[j+1]){
                    t=b[j+1];
                    b[j+1]=b[j];
                    b[j]=t;
                    t=a[j+1];
                    a[j+1]=a[j];
                    a[j]=t;
                }
            }
        }
        System.out.println(a[1]);
        for(int i=0;i<n-1;i++){
            if(b[i]<=a[i+1]){
                count++;
            }
        }

        System.out.println(count);
    }
}
