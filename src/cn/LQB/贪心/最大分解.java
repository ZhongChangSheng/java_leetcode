package cn.LQB.贪心;

import java.util.Scanner;

/**
 * 给出一个正整数n，求一个和最大的序列a0，a1，a2，……，ap，
 * 满足n=a0>a1>a2>……>ap且ai+1是ai的约数，
 * 输出a1+a2+……+ap的最大值
 * 输入仅一行，包含一个正整数n
 * 输出格式 10 a0=10,a1=5,a2=1 ;a1+a2=6
 * 　　一个正整数，表示最大的序列和，即a1+a2+……+ap的最大值
 */
public class 最大分解 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int i=n/2;
        int j=0;
        while (n>1){
            if(n%i==0){
                j=j+i;
                n=i;
                i=n-1;
            }else {
                i--;
            }
        }
        System.out.println(j);
    }

}
