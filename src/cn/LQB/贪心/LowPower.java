package cn.LQB.贪心;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 　  有n个机器，每个机器有2个芯片，每个芯片可以放k个电池。
 * 　　每个芯片能量是k个电池的能量的最小值。
 * 　　两个芯片的能量之差越小，这个机器就工作的越好。
 * 　　现在有2nk个电池，已知它们的能量，我们要把它们放在n个机器上的芯片上，
 * 　　使得所有机器的能量之差的最大值最小。
 *     第一行，两个正整数，n和k。
 * 　　第二行，2nk个整数，表示每个电池的能量。
 *     一行一个整数，表示所有机器的能量之差的最大值最小是多少。
 2 3
 1 2 3 4 5 6 7 8 9 10 11 12
  1
 */
public class LowPower {
    private static int k;
    private static int n;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
         n = sc.nextInt();
        k=sc.nextInt();
        int []a=new int[2*n*k];
        for(int i=0;i<2*n*k;i++)
        {
          a[i]=sc.nextInt();
        }
        Arrays.sort(a);
        int l=0,r=100001;
        while (l<r){
            int mid=(l+r)>>1;
            if(check(mid,a)) r=mid;
            else l=mid+1;

        }
        System.out.println(l);

    }
    public static boolean check(int x,int []a){//检测这个是否可以
        int i,cnt=n;
        for(i=0;cnt<2*n*k-1&&i<2*n*k-1;i++) {
            if(a[i+1]-a[i]<=x) {
                if(2*n*k-i+1<cnt*k*2)
                    return false;
                i++;
                cnt--;
            }
        }
        if(cnt>0){
            return false;
        }else

        return true;

    }


}
