package cn.LQB.贪心;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 　有N个人排队到M个水龙头去打水，他们装满水桶的时间T1,T2……Tn为整数且各不相等，
 * 应如何安排他们的打水顺序才能使他们花费的总时间最少？
 * 　第1行：两个整数n和m，n表示人的个数，m表示水龙头的个数；
 * 　　第2行，n个数分别表示n个人装水的时间
 * 　　数据范围：m<=n/3,n<=1000,t<3000。
 * 一个整数，表示总花费的最少时间。
 * 6 2
   5 4 6 2 1 7

 40
 计算过程1+2+5+7+11+14
 */
public class 排队接水2 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int m=sc.nextInt();
        int []t=new int[n];
        int []wait=new int[m];
        for(int i=0;i<n;i++){
            t[i]=sc.nextInt();
        }
        int sum=0;
        Arrays.sort(t);
        for(int i=0;i<n;){
            for(int j=0;j<m&&i<n;j++,i++){
                wait[j]=wait[j]+t[i];//打水时间等于前面人打水时间加自己打水时间
                sum+=wait[j];
            }
        }
        System.out.println(sum);

    }
}
