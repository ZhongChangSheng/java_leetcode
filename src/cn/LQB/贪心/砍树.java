package cn.LQB.贪心;

import java.util.Scanner;

/**
 * 一排树的高度如下：1 2 5 4 3 3 6 6 2，如果FJ在第三棵树上装炸药（高度为5），那么第二棵树也同样给压倒（高度2<5 )，
 * 第一棵树也同样倒下（高度为1<2 ),再来看另一边第四棵树（高度为4<5）和第五棵树（高度为3<4）同样也给压倒。
 * 剩下的状态为：* * * * * 3 6 6 2，接下来在第7和第8棵树上安装炸药就可以把剩下的树毁掉。
 * 　　请你帮助FJ利用最少的炸药把这些树毁掉。

 */
public class 砍树 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int []a=new int[n];
        for(int i=0;i<n;i++){
            a[i]=sc.nextInt();
        }
        int flag=0;//判断是否单调递增
        for(int i=0;i<n-1;i++){
            flag=0;
            if(a[i+1]<=a[i]){
                flag++;
                System.out.println(i+1);
                int j=0;
                for( j=i;j<n-1;j++){
                    //从i开始遍历直到找到>=a[i],停止将j的值付给i
                    if(a[j+1]>=a[j]){
                        break;
                    }
                }
                i=j;
                if(j==n-2){
                    //代表啊a[j+1]=a[n-1]是最后一个>=a[j],输出n
                    System.out.println(n);
                }
            }
        }
        if (flag==0){//flag为0代表单调递增，直接输出n
            System.out.println(n);
        }
    }
}
