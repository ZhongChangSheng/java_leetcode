package cn.LQB.dp;

import java.util.Scanner;

/**
 * 有一个N x N的方格,每一个格子都有一些金币,只要站在格子里就能拿到里面的金币。
 * 你站在最左上角的格子里,每次可以从一个格子走到它右边或下边的格子里。请问如何走才能拿到最多的金币。
 * 3
 * 1 3 3
 * 2 2 2
 * 3 1 2
 * 11
 */
public class 拿金币 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int dp[][]=new int[n+1][n+1];
        for(int i=0;i<n;i++)
            for(int j=0;j<n;j++){
                dp[i][j]=sc.nextInt();
            }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if(i==0&&j>0){  //判断上边界
                    dp[i][j]=dp[i][j]+dp[i][j-1];
                }
                else if(j==0&&i>0){ //判断左边界
                    dp[i][j]=dp[i][j]+dp[i-1][j];
                }
                else if(i>0){ //判断再中间，必选判断
                    dp[i][j]=dp[i][j]+Math.max(dp[i-1][j],dp[i][j-1]);
                }
            }
        }
        System.out.println(dp[n-1][n-1]);

    }
}
