package cn.LQB.dp;

import java.util.Scanner;

/**
 逗志芃又一次面临了危机。逗志芃的妹子是个聪明绝顶的人，相比之下逗志芃就很菜了。
 现在她妹子要和他玩一个游戏，这个游戏是这样的：一共有n个数（n是偶数）写成一行，
 然后两个人轮流取数，每次只能从最前面或者最后面取走一个数，全部取完则游戏结束，
 之后每个人取走的数的和就是每个人的得分。由于逗志芃妹子很厉害，但他又不想输，
 所以只能找到你了，你要告诉他最多可以得到多少分。
 （注意，妹子智商是maxlongint所以是不会犯错的，每次的策略必然最优，而且逗志芃是先手）
 　第一行一个数n，表示有n个数。
 　　第二行就是进行游戏的n个数。
 */
public class 逗志芃的危机 {
    static int[][] res = new int[1000][1000];//用来存储已经计算过的值，后面又遇到相同问题时可以直接使用，避免重复计算
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        System.out.println(getmore(arr, 0, arr.length - 1, true));
    }

    public static int getmore(int[] arr, int left, int right, boolean flag) {// A拿，越多越好
        if (res[left][right] != 0)// 剪枝，遇到已经存在的值，则直接使用。动态规划的核心思想
            return res[left][right];
        if (left - right == 0)// 题目给出n是偶数，所以结束肯定最后一个是B拿，所以A没有了，返回0
            return 0;
        if (flag) {//A拿，肯定希望越多越好，所以max以后得到的分数
            return res[left][right] = Math.max(arr[left] + getmore(arr, left + 1, right, !flag),
                    arr[right] + getmore(arr, left, right - 1, !flag));
        } else {//轮到B，B也是采取最优策略，所以B希望A后面拿到的分最少，返回的是剩下的min值
            return res[left][right] = Math.min(getmore(arr, left + 1, right, !flag),
                    getmore(arr, left, right - 1, !flag));
        }
    }
}