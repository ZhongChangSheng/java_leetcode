package cn.LQB;

import java.util.Arrays;
import java.util.Scanner;

public class Susu {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int a[]=new int[n];
        boolean[] isPrime=new boolean[100000];
        isPrime[1]=false;//1不是质数
        Arrays.fill(isPrime, 2,100000,true);//全置为true（大于等于2的位置上）
        int m=(int)Math.sqrt(100000);//对range开根号
        for(int i=2;i<m;i++)//注意需要小于等于n
            if(isPrime[i])//查看是不是已经置false过了
                for(int j=i;j*i<100000;j++)//将是i倍数的位置置为false
                    isPrime[j*i]=false;

        for(int i=0;i<n;i++){
            int num=sc.nextInt();
            a[i]=ss(num,isPrime);
        }
        for (int i=0;i<n;i++){
            System.out.println(a[i]);
        }
    }
    public static Integer ss(int num,boolean[] isPrime){
               int count=0;
               for(int i=0;i<num;i++){
                   if(isPrime[num^i]==true)
                         count++;
               }
               return count;
    }

}
