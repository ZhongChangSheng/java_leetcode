package cn.LQB;

import java.util.Scanner;

/**
 * 输入数据第一行为一个整数n，表示芯片个数。
 * 第二行到第n+1行为n*n的一张表，每行n个数据。
 * 表中的每个数据为0或1，在这n行中的第i行第j列（1≤i, j≤n）的数据表示用第i块芯片测试第j块芯片时得到的测试结果，
 * 1表示好，0表示坏，i=j时一律为1（并不表示该芯片对本身的测试结果。芯片不能对本身进行测试）。
 * 1 0 1
 * 0 1 0
 * 1 0 1
 */
public class 芯片测试 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        boolean [][] a=new boolean[n][n];

        for(int i=0;i<n;i++){
            for (int j=0;j<n;j++){
                a[i][j]=((sc.nextInt()==1)?true:false);
            }
        }
        for(int i=0;i<n;i++){
            int count=0;
            for (int j=0;j<n;j++){
                if(a[j][i]==true){
                    count++;
                }
            }
            if(count>n/2){
                System.out.print((i+1)+" ");
            }
        }


    }
}
