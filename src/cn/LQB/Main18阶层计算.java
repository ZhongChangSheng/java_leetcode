package cn.LQB;

import java.util.Scanner;

public class Main18阶层计算 {
    public static void main(String[] args) {
        int[] a = new int[4000];
        Scanner key = new Scanner(System.in);
        int n = key.nextInt();
        for (int i = 0; i < a.length; i++) {
            a[i] = 0;
        }
        a[0] = 1;//把初始值由0改为1

        int jinWei, sum = 0, flag = 0;
        for (int i = 2; i <= n; i++) {
            jinWei = 0;
            for (int j = 0; j < a.length; j++) {   //注意j的循环次数，不是n而是a.length
                sum = jinWei + a[j] * i;
                a[j] = sum % 10;
                jinWei = sum / 10;
            }
        }
         //去掉数组前边多余的0
        for (int i = a.length - 1; i >= 0; i--) {
            if (a[i] != 0) {
                flag = i;
                break;
            }
        }
        for (int i = flag; i >= 0; i--) {
            System.out.print(a[i]);

        }
    }

}

