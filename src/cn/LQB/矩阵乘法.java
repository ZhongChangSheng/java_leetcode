package cn.LQB;

import java.util.Scanner;

public class 矩阵乘法 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int [][] a= new int[n][n];
        for (int i = 0; i < n; i++){
            for (int j=0;j<n;j++){
                a[i][j]=sc.nextInt();
            }
        }
        if(m==0){//矩阵的0次幂是单位矩阵
            for(int i=0;i<n;i++){
                for(int j=0;j<n;j++){
                    if (i==j){
                    System.out.print(1 +" ");}
                    else{
                        System.out.println(0 +" ");}
                }
                System.out.println();
            }

        }
        else if(m==1){//矩阵的一次密是本身
            for(int i=0;i<n;i++){
                for(int j=0;j<n;j++){
                    System.out.print(a[i][j]+" ");
                }
                System.out.println();
            }

        }
        else {
            int [][]b=a;
            for(int i=1;i<m;i++){
                int[][] res=new int[n][n];
                for(int j=0;j<n;j++){
                    for(int k=0;k<n;k++){
                        int count=0;
                        int tem=0;
                        while (count<n){//矩阵AB相乘的公式：C[i][j] = A[i][k] * B[k][j] + ...
                            tem+=a[j][count]*b[count][k];
                            count++;
                        }
                        res[j][k]=tem;
                    }
                }
                a=res;
            }

            for(int i=0;i<n;i++){
                for(int j=0;j<n;j++){
                    System.out.print(a[i][j]+" ");
                }
                System.out.println();
            }
        }

    }

}
