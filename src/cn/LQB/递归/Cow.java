package cn.LQB.递归;

import java.util.Scanner;

/**
 * 有一头母牛，它每年年初生一头小母牛。每头小母牛从第四个年头开始，每年年初也生一头小母牛。
 * 请编程实现在第n年的时候，共有多少头母牛？
 */
public class Cow {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        while (n!=0){
             int sum=Sum(n);
            System.out.println(sum);
            n=sc.nextInt();
        }
        sc.close();
    }
    public static Integer Sum(int n){
         if (n<=4){
             return n;
         }
           return Sum(n-1)+Sum(n-3);
    }
}
