package cn.LQB.递归;

import java.util.Scanner;

/**
 * 　A1 = “A”
 * 　　A2 = “ABA”
 * 　　A3 = “ABACABA”
 * 　　A4 = “ABACABADABACABA”
 * 　　… …
 */
public class FJ的字符串 {
    public static void main(String [] args){
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(Fj(n));

    }
    public static String Fj(int n){
        if(n==1){
            return "A";
        }
        return Fj(n-1)+(char)('A'+n-1)+Fj(n-1);
    }

}
