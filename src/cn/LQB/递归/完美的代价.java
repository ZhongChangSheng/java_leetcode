package cn.LQB.递归;

import java.util.Scanner;

public class 完美的代价 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);int n=sc.nextInt();
        while (sc.hasNext()){

            String str=sc.next();
            if(IsPossible(str)){
                System.out.println(getResult(str));
            }else
                System.out.println("Impossible");
        }
    }
    public static boolean IsPossible(String str){
        int []a=new int[26];
        boolean res=true;
        int count=0;
        for(int i=0;i<str.length();i++){
            a[str.charAt(i)-'a']++;//统计字符串里面字符出现的次数
        }
        for (int i=0;i<26;i++){
            if(a[i]%2==1)//判断字母出现奇数的个数
                count++;
        }
        if(count>1)//出现两个奇数字母，说明不是回文字符串
                res=false;

          return res;
    }
    public static int getResult(String str){
        if(str.length()==1||str.length()==2){
            return 0;
        }
        int tem=str.lastIndexOf(str.charAt(0));//第一个字母最后出现的位置
        if(tem==0){//tem=0,说明第一个字符应该位于中间
            return str.length()/2+getResult(str.substring(1,str.length()));
        }
        else {
            StringBuilder sb=new StringBuilder(str);
            sb.deleteCharAt(0);
            sb.deleteCharAt(tem);
            return str.length()-tem-1+getResult(sb.toString());
        }


    }
}
