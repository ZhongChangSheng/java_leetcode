package cn.LQB.递归;

import java.util.Scanner;

/**
 * An=sin(1–sin(2+sin(3–sin(4+...sin(n))...)
 * 　　Sn=(...(A1+n)A2+n-1)A3+...+2)An+1
 * 　　FJ想让奶牛们计算Sn的值，请你帮助FJ打印出Sn的完整表达式，以方便奶牛们做题。
 */
public class Sine之舞 {
    public static void main(String[] args) {
       Scanner sc=new Scanner(System.in);
       int n=sc.nextInt();
        System.out.println(Sn(n,n));
    }
    public static StringBuffer An(int n){
          StringBuffer sb=new StringBuffer();
          if(n==1){
          sb.append("sin(1");
          }
          else {
              sb.append(An(n-1)).append(n%2==1? "+":"-").append("sin("+n);
          }
        while(n-- > 0)
            sb.append(")");
        return sb;

    }
    public static StringBuffer Sn(int n,int m){
           StringBuffer sb=new StringBuffer();
           if(n==1){
               sb.append(An(1)).append("+"+(m));
           }
           else {
               sb.append("(").append(Sn(n-1,m)).append(")").append(An(n)).append("+" + (m-n+1));
           }
          return sb;
    }
}
