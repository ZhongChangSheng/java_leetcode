package cn.LQB.搜索;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 逗志芃在干了很多事情后终于闲下来了，然后就陷入了深深的无聊中。不过他想到了一个游戏来使他更无聊。
 * 他拿出n个木棍，然后选出其中一些粘成一根长的，然后再选一些粘成另一个长的，
 * 他想知道在两根一样长的情况下长度最长是多少。
 * 第一行一个数n，表示n个棍子。第二行n个数，每个数表示一根棍子的长度。
 4
 1 2 3 1
 */
public class 无聊的逗 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int[]a=new int[n];
        int sum=0;
        for(int i=0;i<n;i++){
            a[i]=sc.nextInt();
            sum+=a[i];
        }
        Arrays.sort(a);
        sum=sum/2;
        int res=0;
        for(int i=n-1;i>=0;i--){
            if(sum-res>=a[i]){
                res+=a[i];
            }
        }
        System.out.println(res);
    }
}
