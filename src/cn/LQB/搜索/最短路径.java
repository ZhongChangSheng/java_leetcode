package cn.LQB.搜索;

import java.util.Scanner;

public class 最短路径 {
    static int[][]e=new int[100][100];
    static int[]vi=new int[100];//标记是否被访问
    static int n,m,min=Integer.MAX_VALUE;
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        n=sc.nextInt();
        m=sc.nextInt();
        for(int i=1;i<=n;i++){
            for(int j=1;j<=m;j++){
                if(i==j){
                    e[i][j]=0;
                }
                else {
                    e[i][j]=Integer.MAX_VALUE;
                }
            }
        }
        for(int i=1;i<=m;i++){
            int a=sc.nextInt();
            int b=sc.nextInt();
            int c=sc.nextInt();
            e[a][b]=c;
        }
        dfs(1,0);
        System.out.println(min);
    }
    //cur当前位置，dis路径长度
    public static void dfs(int cur,int dis){
        if (dis>min){
            return;
        }
        //如果到最后一个节点
            if(cur==n){
                //如果路径小于最短路径，交换值
                if(dis<min){
                    min=dis;
                    return;
                }
            }
            for(int i=1;i<=n;i++){
                if(e[cur][i]!=Integer.MAX_VALUE&&vi[i]==0){
                    vi[i]=1;
                    dfs(i,dis+e[cur][i]);
                    vi[i]=0;//回溯
                }
            }
            return;
    }
}
