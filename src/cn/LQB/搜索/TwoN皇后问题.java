package cn.LQB.搜索;

import java.util.Scanner;

/**
 * 给定一个n*n的棋盘，棋盘中有一些位置不能放皇后。
 * 现在要向棋盘中放入n个黑皇后和n个白皇后，
 * 使任意的两个黑皇后都不在同一行、同一列或同一条对角线上，
 * 任意的两个白皇后都不在同一行、同一列或同一条对角线上。问总共有多少种放法？n小于等于8。
 */
public class TwoN皇后问题 {
    static int n;
    static int count=0;
    static int []h;//黑皇后所在位置
    static int []b;//白皇后所在位置
    static int hhs=2;//代表黑皇后
    static int bhs=3;//代表白皇后
    static int[][] chess;
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
         n=sc.nextInt();
        chess=new int[n][n];
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                chess[i][j]=sc.nextInt();
            }
        }
        b=new int[n];
        h=new int[n];
        dfs(hhs,h,0);
        System.out.println(count);
    }
    private static void dfs(int color,int[] address,int line){
        //最后一行停止搜索
        if(line==n){
            if(color==hhs){
                //如果颜色是黑皇后，就放白皇后
                dfs(bhs,b,0);
            }
            else {
            count++;
            }
            return;
        }
        for(int i=0;i<n;i++) {
            if (chess[line][i] == 1 && check(address,line, i)) {
                //如果棋盘这个位置为空，并且能放棋子
                //记录该皇后的位置
                address[line]=i;//表示第line行，第i列
                chess[line][i]=color;//放入代表颜色棋子
                dfs(color,address,line+1);//继续找下一列
                chess[line][i]=1;//回溯
            }
        }
    }
    private static boolean check(int[] address,int x,int y){
             for(int i=0;i<x;i++){
                 if(address[i]==y){
                     //表示在同一列不能放棋子
                     return false;
                 }
                 if(i+address[i]==x+y){
                     //表示在上对角线不能放
                     return false;
                 }
                 if(i-address[i]==x-y){
                     //表示在下对角线不能放
                     return false;
                 }
             }
             return true;
    }

}
