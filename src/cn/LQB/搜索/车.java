package cn.LQB.搜索;

import java.util.Scanner;

public class 车 {
    static int count=1;
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int []a=new int[n+1];
        dfs(1,a);
        System.out.println(count);
    }

    private static void dfs(int line, int[] a) {
       if(line>a.length-1){
           return;
       }
       for(int i=1;i<a.length;i++){
           //如果a[i]=0,说明这个位置课以放置车
           if(a[i]==0){
               a[i]=1;
               count++;
               dfs(line+1,a);
               a[i]=0;//回溯
           }
       }
       dfs(line+1,a);
    }
}
