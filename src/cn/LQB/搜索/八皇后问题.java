package cn.LQB.搜索;

import java.util.Scanner;

public class 八皇后问题 {
    static int n;
    static int count=0;
    static int []a=new int[10];
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        n=sc.nextInt();
        dfs(1);
        System.out.println(count);
    }
    private static void dfs(int line){
        //最后一行停止搜索
        if(line>n){
            count++;
            return;
        }
        for(int i=1;i<=n;i++){
            if(check(line,i)){//判断第line行，第i列是否能放棋子

                a[line]=i;
                dfs(line+1);
                a[line]=0;//回溯
            }
        }

    }
    private static boolean check(int x,int y){
        for(int i=1;i<x;i++){
            if(a[i]==y){//表示在同一列不能放棋子
                return false;
            }
            if(i+a[i]==x+y){
                //表示在上对角线不能放
                return false;
            }
            if(i-a[i]==x-y){
                //表示在下对角线不能放
                return false;
            }
        }
        return true;
    }
}
