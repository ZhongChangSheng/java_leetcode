package cn.LQB.搜索;

import java.util.Scanner;

public class test {
    static int n;
    static int[]hhh;
    static int[]bhh;
    static int[][] chess;
    static int count=0;
    static int h=2;
    static int b=3;
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        n=sc.nextInt();
        chess=new int[n][n];
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                chess[i][j]=sc.nextInt();
            }
        }
        hhh=new int[n];
        bhh=new int[n];
        dfs(hhh,0,h);
        System.out.println(count);
    }

    private static void dfs(int[] address,int line,int color) {
        if(line==n){
           if(color==h){
               dfs(bhh,0,b);
           }else {
               count++;

           }
            return;
        }
        for(int i=0;i<n;i++){
            if(check(address,line,i)&&chess[line][i]==1){//chess[line][i]==1代表能放置棋子
                address[line]=i;//表示放置在第line行，第i列
                chess[line][i]=color;//表示放置的棋子颜色
                dfs(address,line+1,color);
                chess[line][i]=1;

            }
        }

    }

    private static boolean check(int[]address,int x, int y) {
        for(int i=1;i<=x;i++){
            if(address[i]==y){
                return false;
            }
            if(i+address[i]==x+y){
                return false;
            }
            if(i-address[i]==x-y){
                return false;
            }
        }
        return true;
    }
}
