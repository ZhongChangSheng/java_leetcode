package cn.LQB.搜索;

import java.util.Scanner;

/**
 * 在一个n*n的棋盘中，每个格子中至多放置一个车，
 * 且要保证任何两个车都不能相互攻击，有多少中放法(车与车之间是没有差别的)
 * 一个车都不放为1种，放置一个车有4种，放置2个车有2种。
 */
public class 车的放置 {
    static int count = 1;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] flag = new int[n];
        dfs(0, flag);            //从第0行开始搜索
        System.out.println(count);
    }

    public static void dfs(int line, int[] flag) {
        if (line > flag.length - 1) {
            return;
        }
        for (int i = 0; i < flag.length; i++) {
            //判断当前列是否有车放下
            if (flag[i] == 0) {
                flag[i] = 1;
                count++;
                dfs(line + 1, flag);
                flag[i] = 0;
            }
        }
        dfs(line + 1, flag);
    }
}
