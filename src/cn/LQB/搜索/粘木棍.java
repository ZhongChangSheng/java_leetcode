package cn.LQB.搜索;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 有N根木棍，需要将其粘贴成M个长木棍，使得最长的和最短的的差距最小。
 3 2
 10 20 40
 10
 */
public class 粘木棍 {
    static int n;
    static int m;
    static int []a=new int[8];
    static int res=Integer.MAX_VALUE;
    static int ma=Integer.MAX_VALUE;
    static int mi=Integer.MIN_VALUE;
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        n=sc.nextInt();
        m=sc.nextInt();
        for(int i=0;i<n;i++){
            a[i]=sc.nextInt();
        }
        dfs(n);
        System.out.println(res);
    }
    public static void dfs(int u){
        if(u==m){
            ma=a[0];
            mi=a[0];
            for(int i=0;i<m;i++){
                if(a[i]>ma){
                    ma=a[i];
                }
                if(a[i]<mi){
                    mi=a[i];
                }
            }
            res=Math.min(res,ma-mi);
            return;
        }
        for(int i=0;i<u;i++){
            for(int j=i+1;j<u;j++){
                a[i]+=a[j];
                int tem=a[j];
                a[u-1]=a[j];
                a[j]=tem;
                dfs(u-1);
                int t=a[j];
                a[u-1]=a[j];
                a[j]=tem;
                a[i]-=a[j];
            }
        }
    }

}
