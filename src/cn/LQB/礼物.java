package cn.LQB;

import java.util.Scanner;


/**
 * JiaoShou在爱琳大陆的旅行完毕，即将回家，为了纪念这次旅行，他决定带回一些礼物给好朋友。
 * 　　在走出了怪物森林以后，JiaoShou看到了排成一排的N个石子。
 * 　　这些石子很漂亮，JiaoShou决定以此为礼物。
 * 　　但是这N个石子被施加了一种特殊的魔法。
 * 　　如果要取走石子，必须按照以下的规则去取。
 * 　　每次必须取连续的2*K个石子，并且满足前K个石子的重量和小于等于S，后K个石子的重量和小于等于S。
 * 　　由于时间紧迫，Jiaoshou只能取一次。
 * 　　现在JiaoShou找到了聪明的你，问他最多可以带走多少个石子。
 */
public class 礼物 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int s=sc.nextInt();
        int a[]=new int[n+1];
        int b[]=new int[n+1];
        for(int i=1;i<=n;i++){
            a[i]=sc.nextInt();
            System.out.print(" ");
            b[i]=b[i-1]+a[i];
        }
        int l=1;
        int r=1000000;
        while(l<r){
            int mid=(l+r+1)>>1;
            if(check(mid,n,b,s))
                l=mid;
            else
                r=mid-1;
        }
        System.out.println(2*l);
    }
    public static boolean check(int tem,int n,int b[],int s){
        for(int i=tem;i<=n-tem;i++){
            if(b[i]-b[i-tem]<=s&&b[i+tem]-b[i]<=s){//当答案存在时 返回true
                return true;
            }
        }
        return false;
    }
}
