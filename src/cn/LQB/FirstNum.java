package cn.LQB;

import java.util.Scanner;

/**
 * 给出一个包含n个整数的数列，问整数a在数列中的第一次出现是第几个。
 */
public class FirstNum {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int a[]=new int[n];
        for(int i=0;i<n;i++){
            a[i]=sc.nextInt();
        }
        int b=sc.nextInt();
        for(int i=0;i<n;i++){
            if(a[i]==b){
                System.out.println(i+1);
                break;
            }
            if(i==n-1){
                System.out.println(-1);
            }
        }
    }
}
