package cn.LQB;

import java.util.Scanner;

public class 时间显示 {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        long ms = in.nextLong();
        long hour_div = 60 * 60 * 1000;
        long min_div = 60 * 1000;
        long hour = (ms / hour_div) % 24;
        ms %= hour_div;
        long min = ms / min_div;
        ms %= min_div;
        long sec = ms / 1000;
        System.out.printf("%02d:%02d:%02d\n", hour, min, sec);
    }
}
