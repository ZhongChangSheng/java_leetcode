package cn.LQB;

import java.util.Scanner;

public class MainN2 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int dp[][]=new int[n+1][n+1];
        for(int i=0;i<n;i++)
            for(int j=0;j<n;j++){
                dp[i][j]=sc.nextInt();
            }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if(i==0&&j>0){  //判断上边界
                    dp[i][j]=dp[i][j]+dp[i][j-1];
                }
                else if(j==0&&i>0){ //判断左边界
                    dp[i][j]=dp[i][j]+dp[i-1][j];
                }
                else if(i>0){ //判断再中间，必选判断
                    dp[i][j]=dp[i][j]+Math.max(dp[i-1][j],dp[i][j-1]);
                }
            }
        }
        System.out.println(dp[n-1][n-1]);

    }
}
