package cn.LQB;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        String str[]=new String[n];
        for (int i=0;i<n;i++){
           str[i]=sc.next();
        }
        for(int i=0;i<n;i++){
            String s= hexToBin(str[i]).toString();
            String s1 = binToOct(s);
            if(s1.startsWith("0")){
                s1=s1.substring(1);//substring(int beginIndex)返回一个新的字符串，它是此字符串的一个子字符串。
            }
            if(s1.startsWith("0")){
                s1=s1.substring(1);//substring(int beginIndex)返回一个新的字符串，它是此字符串的一个子字符串。
            }
            System.out.println(s1);
        }
    }
    private static StringBuffer hexToBin(String str) {
        int length = str.length();
        int start = 0;
        int end = 1;
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < length; i++) {
            /*
             * substring(int beginIndex,int endIndex)返回一个新字符串，它是此字符串的一个子字符串。
             *  该子字符串从指定的 beginIndex 处开始，直到索引 endIndex - 1 处的字符。
             *   因此，该子字符串的长度为 endIndex-beginIndex。
             */
            String subStr = str.substring(start, end); //抽取一个十六进制字符
            start++;
            end++;
            String s = slze(subStr); 				//将抽取的十六进制字符转换成二进制字符
            result.append(s);
        }
        return result;
    }
    private static String binToOct(String str) {
        int length = str.length();
        /*
         * 二进制字符串的长度不是3的整数倍的时候，就要在字符串的前面补上相应个数的 ‘0’
         * 来让二进制字符串的长度变成3的整数倍
         */
        if(length % 3 == 1) {
            str= "00"+str;
        } else if(length % 3 == 2) {
            str = "0"+str;
        }

        int start = 0;
        int end = 3;
        StringBuffer sb = new StringBuffer();
        for(int i=0; i<str.length()/3; i++) {
            String subStr = str.substring(start, end); //抽取三个二进制字符
            start += 3;
            end += 3;
            String s = ezb(subStr); //将抽取的二进制字符串转换成八进制字符
            sb.append(s);
        }
        return sb.toString();
    }
    public static String slze(String str){
        String result=" ";
        switch (str){
            case "0":result="0000";break;
            case "1":result="0001";break;
            case "2":result="0010";break;
            case "3":result="0011";break;
            case "4":result="0100";break;
            case "5":result="0101";break;
            case "6":result="0110";break;
            case "7":result="0111";break;
            case "8":result="1000";break;
            case "9":result="1001";break;
            case "A":result="1010";break;
            case "B":result="1011";break;
            case "C":result="1100";break;
            case "D":result="1101";break;
            case "E":result="1110";break;
            case "F":result="1111";break;
            default:break;
        }
        return result;
    }
    public static String ezb(String str){
        String result=" ";
        switch (str){
            case "000":result="0";break;
            case "001":result="1";break;
            case "010":result="2";break;
            case "011":result="3";break;
            case "100":result="4";break;
            case "101":result="5";break;
            case "110":result="6";break;
            case "111":result="7";break;
            default:break;
        }
        return result;
    }
}
