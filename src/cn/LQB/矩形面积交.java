package cn.LQB;

import java.util.Scanner;

/**
 * 平面上有两个矩形，它们的边平行于直角坐标系的X轴或Y轴。
 * 对于每个矩形，我们给出它的一对相对顶点的坐标，请你编程算出两个矩形的交的面积。
 */
public class 矩形面积交 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        double x1=sc.nextDouble();
        double y1=sc.nextDouble();
        double x2=sc.nextDouble();
        double y2=sc.nextDouble();
        double x3=sc.nextDouble();
        double y3=sc.nextDouble();
        double x4=sc.nextDouble();
        double y4=sc.nextDouble();
        double w1=min(max(x1,x2),max(x3,x4));
        double w2=max(min(x1,x2),min(x3,x4));
        double l1=min(max(y1,y2),max(y3,y4));
        double l2=max(min(y1,y2),min(y3,y4));
        double w=w1-w2;
        System.out.println(w);
        double l=l1-l2;
        if(w>0&&l>0){
            double s=w*l;
            System.out.printf("%.2f",s);
        }else {
            System.out.println(0.00);
        }
    }
    public static double max(double a,double b){
        return ((a>b)?a:b);
    }
    public static double min(double a,double b){
        return ((a<b)?a:b);
    }
}