package cn.LQB;

import java.util.Scanner;

/**
 * 问题描述
 * 给定一个年份，判断这一年是不是闰年。
 * 当以下情况之一满足时，这一年是闰年：
 * 1. 年份是4的倍数而不是100的倍数；
 * 2. 年份是400的倍数。
 * 其他的年份都不是闰年。
 */
public class Main14 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        if(n%4==0 && n%100!=0 || n%400==0){
            System.out.println("yes");
        }
        else {
            System.out.println("no");
        }

    }
}
