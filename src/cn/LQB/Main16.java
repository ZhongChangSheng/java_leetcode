package cn.LQB;

import java.util.Scanner;

public class Main16 {
    public static void main(String args[]) {
        double PI=3.14159265358979323;
        Scanner sc=new Scanner(System.in);
        int r=sc.nextInt();
        String s=String.format("%.7f",PI*r*r);//保留7位小数
        System.out.println(s);
    }
}
