package cn.LQB.基础;

/**
 * 插入排序的高效改版
 * 首次选择的增量step=n/2;
 * 然后将序列分成step个组
 *
 */
public class 希尔排序 {
    public static void main(String[] args) {
        int []a={1,4,7,3,49,5};
        paixu(a);
    }
    public static void paixu(int []arr) {
        int gap = arr.length;
        while (gap>1)
        {
            //每次对gap折半操作
            gap = gap / 2;
            //单趟排序
            for (int i = 0; i <arr.length - gap; ++i)
            {
                int end = i;
                int tem = arr[end + gap];
                while (end >= 0)
                {
                    if (tem < arr[end])
                    {
                        arr[end + gap] = arr[end];
                        end -= gap;
                    }
                    else
                    {
                        break;
                    }
                }
                arr[end + gap] = tem;
            }
        }
        for (int i : arr) {
            System.out.print(i+" ");
        }
    }
}
