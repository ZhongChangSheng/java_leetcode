package cn.LQB.基础;

/**
 * abcde 排列组合
 */
public class 全排列 {
    public static void main(String[] args) {
        String a="ABCDE";
        dfs(a.toCharArray(),0);
    }

    private static void dfs(char a[], int k) {
        if(k==a.length-1){//递归出口
            System.out.println(String.valueOf(a));
            return;
        }
        for(int i=k;i<a.length;i++){
            char t=a[k];
            a[k]=a[i];
            a[i]=t;
            dfs(a,k+1);
            char m=a[k];
            a[k]=a[i];
            a[i]=m;
        }
    }

}
