package cn.LQB.基础;

/**
 *将数组分为两个区域:已排序，未排序
 * 取未排序的区域的元素，在已排序的区域找到合适的位置
 */
public class 插入排序 {
    public static void main(String[] args) {
        int []a={1,4,7,3,49,5};
        paixu(a);
    }
    public static void paixu(int a[]){
        for(int i=1;i<a.length;i++){
            int j=i-1;
            int value=a[i];
            for(;j>=0;j--){
                if(a[j]>value){
                    a[j+1]=a[j];
                }else {
                    break;
                }
            }
            a[j+1]=value;
        }
        for (int i : a) {
            System.out.print(i+" ");
        }
    }
}
