package cn.LQB.基础;

/**
 * 选择排序，从未排序区域选择最小的一个放在已排序的后面
 */
public class 选择排序 {
    public static void main(String[] args) {
        int []a={1,4,7,3,49,5};
        paixu(a);
    }
    public static void paixu(int []a){
        int minIndex;//记录未排序区域的最小值的下标
        for(int i=0;i<a.length-1;i++){
            minIndex=i;
            for(int j=i+1;j<a.length;j++){
                if(a[j]<a[minIndex]){
                    minIndex=j;
                }
            }
            if(i!=minIndex){
                //表明最小值以改变
                int tem=a[i];
                a[i]=a[minIndex];
                a[minIndex]=tem;
            }
        }
        for (int i : a) {
            System.out.print(i+" ");
        }
    }
}
